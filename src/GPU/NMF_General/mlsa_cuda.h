/***************************************************************************
 *   Copyright (C) 2014 by PIR (University of Oviedo) and                  *
 *   INCO2 (Polytechnic University of Valencia) groups.                    *
 *    nmfpack@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************
*/
/**
 *  \file    mlsa_cuda.h
 *  \brief   Header file for using the mlsa algorithm using cuda functions with GPUs
 *  \author  Information Retrieval and Parallel Computing Group (IRPCG)
 *  \author  University of Oviedo, Spain
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \author  Contact: nmfpack@gmail.com
 *  \date    04/11/14
*/
#ifndef MLSA_CUDA_H
#define MLSA_CUDA_H

#include "utils_cuda.h"

#define UpdateAll 1
#define UpdateW   2
#define UpdateH   3


int dmlsa_cuda(const int m, const int n, const int k, const double *A, double *W, double *H, const int uType, const int nIter);
int smlsa_cuda(const int m, const int n, const int k, const float  *A, float  *W, float  *H, const int uType, const int nIter);

/*support functions */
void ddotdiv_cuda(const int n, const double *x, const double *y, double *z, cudaStream_t stream);
void sdotdiv_cuda(const int n, const float  *x, const float  *y, float  *z, cudaStream_t stream);

/*kernels*/
__global__ void vddotdiv_cuda(const int n, const double* __restrict__ x, const double* __restrict__ y, double *z);
__global__ void vsdotdiv_cuda(const int n, const float*  __restrict__ x, const float*  __restrict__ y, float  *z);

#endif
