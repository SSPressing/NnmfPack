/**
 *  \file    dfhals_cpu.h
 *  \brief   Header file for using the HALS algorithm  with GPUs
 *  \author  P. San Juan
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \date    09/05/18
*/
#ifndef HALS_CUDA_H
#define HALS_CUDA_H

#include "utils_cuda.h"

int dfhals_cuda(const int m, const int n, const int k, const double *A, double *W, double *H, const int nIter); 
int sfhals_cuda(const int m, const int n, const int k, const float *A,  float *W,  float *H,  const int nIter); 

#endif
