/***************************************************************************
 *   Copyright (C) 2014 by PIR (University of Oviedo) and                  *
 *   INCO2 (Polytechnic University of Valencia) groups.                    *
 *    nmfpack@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************
*/
/**
 *  \file    utils_cpu.c
 *  \brief   Some auxiliar functions. Double and simple precision for CPU and MIC.
 *  \author  Information Retrieval and Parallel Computing Group (IRPCG)
 *  \author  University of Oviedo, Spain
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \author  Contact: nmfpack@gmail.com
 *  \date    04/11/14
*/
#include "utils_cpu.h"


/**
 *  \fn   void dmemset_cpu(const int n, double *__restrict__ x, const double val)
 *  \brief This function fills all positions of x with val
 *  \param n:   (input)  Number of elements of x
 *  \param x:   (output) Double precision output matrix (1D column-major) or vector
 *  \param val: (input)  Double precision value
*/
void dmemset_cpu(const int n, double *__restrict__ x, const double val)
{
   int i;
  
   #ifdef With_ICC
     #pragma loop_count min=1024
     #pragma omp simd
   #else
     #ifdef With_OMP
       #pragma omp parallel for
     #endif
   #endif
   for (i=0; i<n; i++) { x[i]=val; }
}


/**
 *  \fn   void smemset_cpu(const int n, float *__restrict__ x, const float val)
 *  \brief This function fills all positions of x with val
 *  \param n:   (input)  Number of elements of x
 *  \param x:   (output) Simple precision output matrix (1D column-major) or vector
 *  \param val: (input)  Simple precision value
*/
void smemset_cpu(const int n, float *__restrict__ x, const float val)
{
   int i;

   #ifdef With_ICC
     #pragma loop_count min=1024
     #pragma omp simd
   #else
     #ifdef With_OMP
       #pragma omp parallel for
     #endif
   #endif
   for (i=0; i<n; i++) { x[i]=val; }
}


/**
 *  \fn   void imemset_cpu(const int n, int *__restrict__ x, const int val)
 *  \brief This function fills all positions of x with val
 *  \param n:   (input)  Number of elements of x
 *  \param x:   (output) Integer output matrix (1D column-major) or vector
 *  \param val: (input)  Integer value
*/
void imemset_cpu(const int n, int *__restrict__ x, const int val)
{
   int i;

   #ifdef With_ICC
     #pragma loop_count min=1024
     #pragma omp simd
   #else
     #pragma omp parallel for
   #endif
   for (i=0; i<n; i++) { x[i]=val; }
}


/**
 *  \fn   void ddiv_cpu(const int n, const double *x, const double *y,  double *__restrict__ z)
 *  \brief This function calls the appropiate funtions to performs double precision
 *         element-wise  z[i]=x[i]/y[i] for all positions of x and y
 *  \param n: (input) Number of elements of x, y and z
 *  \param x: (input) Double precision input vector/matrix (1D column-major)
 *  \param y: (input) Double precision input vector/matrix (1D column-major)
 *  \param z: (inout) Double precision input/output vector/ matrix (1D column-major)
*/
void ddiv_cpu(const int n, const double *x, const double *y,  double *__restrict__ z)
{
   #ifdef With_MKL
     vdDiv(n, x, y, z);
   #else
     int i;
     #ifdef With_ICC
       #pragma loop_count min=32
       #pragma omp simd
     #else
       #pragma omp parallel for
     #endif
     for (i=0; i<n; i++)
     {
       #ifdef With_Check
         /* Here we can have NaN and Inf if y(i) and/or x(i)=0 */
         assert(isfinite(y[i]));
         z[i]=x[i] / y[i];
       #else
         z[i]=x[i] / y[i];
       #endif
     }
   #endif
}


/**
 *  \fn    void sdiv_cpu(const int n, const float *x, const float * y, float *__restrict__ y)
 *  \brief This function calls the appropiate funtions to performs simple precision
 *         element-wise  y[i]=x[i]/y[i] for all positions of x and y
 *  \param n: (input) Number of elements of x and y
 *  \param x: (input) Simple precision input vector/matrix (1D column-major)
 *  \param y: (inout) Simple precision input/output vector/ matrix (1D column-major)

*/
void sdiv_cpu(const int n, const float *x , const float *y, float *__restrict__ z)
{
   #ifdef With_MKL
     vsDiv(n, x, y, z);
   #else
     int i;
     #ifdef With_ICC
       #pragma loop_count min=32
       #pragma omp simd
     #else
     #ifdef With_OMP
       #pragma omp parallel for
     #endif
     #endif
     for (i=0; i<n; i++)
     {
       #ifdef With_Check
         /* Here we can have NaN and Inf if y(i) and/or x(i)=0 */
         assert(isfinite(y[i]));
         z[i]=x[i] / y[i];
       #else
         z[i]=x[i] / y[i];
       #endif
     }
   #endif
}


/**
 *  \fn    void ddotdiv_cpu(const int n, const double *x, const double *y, double *__restrict__ z)
 *  \brief This function calls the appropiate funtions to performs double precision
 *         element-wise  z[i]=z[i]*x[i]/y[i] for all positions of x, y and z
 *  \param n: (input) Number of elements of x, y and z
 *  \param x: (input) Double precision input vector/matrix (1D column-major)
 *  \param y: (input) Double precision input vector/matrix (1D column-major)
 *  \param z: (inout) Double precision input/output vector/matrix (1D column-major)
*/
void ddotdiv_cpu(const int n, const double *x, const double *y, double *__restrict__ z)
{
   #ifdef With_MKL
     vdMul(n, z, x, z);
     vdDiv(n, z, y, z);
   #else
     int i;
     #ifdef With_ICC
       #pragma loop_count min=32
       #pragma omp simd
     #else
       #ifdef With_OMP
         #pragma omp parallel for
       #endif
     #endif
     for (i=0; i<n; i++)
     {
       #ifdef With_Check
         /* Here we can have NaN and Inf if y(i) and/or x(i) and/or z[i]=0 */
         z[i]=z[i] * x[i] / y[i];
         assert(isfinite(z[i]));
       #else
         z[i]=z[i] * x[i] / y[i];
       #endif
     }
   #endif
}


/**
 *  \fn    void sdotdiv_cpu(const int n, const float *x, const float *y, float *__restrict__ z)
 *  \brief This function calls the appropiate funtions to performs simple precision
 *         element-wise  z[i]=z[i]*x[i]/y[i] for all positions of x, y and z
 *  \param n: (input) Number of elements of x, y and z
 *  \param x: (input) Simple precision input vector/matrix (1D column-major)
 *  \param y: (input) Simple precision input vector/matrix (1D column-major)
 *  \param z: (inout) Simple precision input/output vector/matrix (1D column-major)
*/
void sdotdiv_cpu(const int n, const float *x, const float *y, float *__restrict__ z)
{
   #ifdef With_MKL
     vsMul(n, z, x, z);
     vsDiv(n, z, y, z);
   #else
     int i;
     #ifdef With_ICC
       #pragma loop_count min=32
       #pragma omp simd
     #else
       #ifdef With_OMP
         #pragma omp parallel for
       #endif
     #endif
     for (i=0; i<n; i++)
     {
       #ifdef With_Check
         /* Here we can have NaN and Inf if y(i) and/or x(i) and/or z[i]=0 */
         z[i]=z[i] * x[i] / y[i];
         assert(isfinite(z[i]));
       #else
         z[i]=z[i] * x[i] / y[i];
       #endif
     }
   #endif
}


/**
 *  \fn   void dmult_cpu(const int n, const double *x, const double *y,  double *__restrict__ z)
 *  \brief This function calls the appropiate funtions to performs double precision
 *         element-wise  z[i]=x[i]*[i] for all positions of x and y
 *  \param n: (input) Number of elements of x and y
 *  \param x: (input) Double precision input vector/matrix (1D column-major)
 *  \param y: (input) Double precision input vector/matrix (1D column-major)
 *  \param z: (inout) Double precision input/output vector/ matrix (1D column-major)
*/
void dmult_cpu(const int n, const double *x, const double *y,  double *__restrict__ z)
{
   #ifdef With_MKL
     vdMul(n, x, y, z);
   #else
     int i;
     #ifdef With_ICC
       #pragma loop_count min=32
       #pragma omp simd
     #else
       #pragma omp parallel for
     #endif
     for (i=0; i<n; i++) { z[i]=x[i] * y[i]; }
   #endif
}


/**
 *  \fn   void smult_cpu(const int n, const float *x, const float *y, float *__restrict__ z)
 *  \brief This function calls the appropiate funtions to performs simple precision
 *         element-wise  z[i]=x[i]*[i] for all positions of x and y
 *  \param n: (input) Number of elements of x and y
 *  \param x: (input) Simple precision input vector/matrix (1D column-major)
 *  \param y: (input) Simple precision input vector/matrix (1D column-major)
 *  \param z: (inout) Simple precision input/output vector/ matrix (1D column-major)
*/
void smult_cpu(const int n, const float *x, const float *y, float *__restrict__ z)
{
   #ifdef With_MKL
     vsMul(n, x, y, z);
   #else
     int i;
     #ifdef With_ICC
       #pragma loop_count min=32
       #pragma omp simd
     #else
       #pragma omp parallel for
     #endif
     for (i=0; i<n; i++) { z[i]=x[i] * y[i]; }
   #endif
}


/**
 *  \fn    void dsub_cpu(const int n, const double *x, const double *y, double *__restrict__ z)
 *  \brief This function performs double precision element-wise substraction z[i]=x[i]-y[i]
 *  \param n: (input) Number of elements of x, y and z
 *  \param x: (input) Double precision input vector/matrix (1D column-major)
 *  \param y: (input) Double precision input vector/matrix (1D column-major)
 *  \param z: (inout) Double precision input/output vector/ matrix (1D column-major)
*/
void dsub_cpu(const int n, const double *x, const double *y, double *__restrict__ z)
{
   #ifdef With_MKL
     vdSub(n, x, y, z);
   #else
     int i;
     #ifdef With_ICC
       #pragma loop_count min=512
       #pragma omp simd
     #else
       #pragma omp parallel for
     #endif
     for (i=0; i<n; i++) { z[i] = x[i] - y[i]; }
   #endif
}


/**
 *  \fn    void ssub_cpu(const int n, const float *x, const float *y, float *__restrict__ y)
 *  \brief This function performs simple precision element-wise substraction z[i]=x[i]-y[i]
 *  \param n: (input) Number of elements of x and y
 *  \param x: (input) Simple precision input vector/matrix (1D column-major)
 *  \param y: (inout) Simple precision input/output vector/matrix (1D column-major)
*/
void ssub_cpu(const int n, const float *x, const float *y, float *__restrict__ z)
{
   #ifdef With_MKL
     vsSub(n, x, y, z);
   #else
     int i;
     #ifdef With_ICC
       #pragma loop_count min=512
       #pragma omp simd
     #else
     #ifdef With_OMP
       #pragma omp parallel for
     #endif
     #endif
     for (i=0; i<n; i++) { z[i] = x[i] - y[i]; }
   #endif
}


/**
 *  \fn    void  dmatSub_cpu(const int m, const int n, const double *B, const int ldb, double *__restrict__ A, const int lda)
 *  \brief This function performs double precision element-wise  matrix substraction A[i]=A[i]-B[i]
 *  \param m:   (input) Number of rows of A ans B
 *  \param n:   (input) Number of columns of A ans B
 *  \param B:   (input) Double precision input matrix (1D column-major)
 *  \param ldb: (input) Leading dimension of matrix B
 *  \param A:   (inout) Double precision input/output matrix (1D column-major)
 *  \param lda: (input) Leading dimension of matrix A
*/
void dmatSub_cpu(const int m, const int n, const double *B, const int ldb, double *__restrict__ A, const int lda)
{
   int j;
   #ifdef With_ICC
     #pragma loop_count min=512
     #pragma omp simd
   #else
     #pragma omp parallel for
   #endif
   for (j=0; j<n; j++)
     cblas_daxpy(m, -1, &B[j*ldb], 1, &A[j*lda], 1);
}


/**
 *  \fn    void  smatSub_cpu(const int m, const int n, const float *B, const int ldb, float *__restrict__ A, const int lda)
 *  \brief This function performs double precision element-wise  matrix substraction A[i]=A[i]-B[i]
 *  \param m:   (input) Number of rows of A ans B
 *  \param n:   (input) Number of columns of A ans B
 *  \param B:   (input) Simple precision input matrix (1D column-major)
 *  \param ldb: (input) Leading dimension of matrix B
 *  \param A:   (inout) Simple precision input/output matrix (1D column-major)
 *  \param lda: (input) Leading dimension of matrix A
*/
void smatSub_cpu(const int m, const int n, const float *B, const int ldb, float *__restrict__ A, const int lda)
{
   int j;
   #ifdef With_ICC
     #pragma loop_count min=512
     #pragma omp simd
   #else
     #pragma omp parallel for
   #endif
   for (j=0; j<n; j++)
     cblas_saxpy(m, -1, &B[j*ldb], 1, &A[j*lda], 1);
}


/**
 *  \fn     void dlarngenn_cpu(const int m, const int n, const int seed, double *X)
 *  \brief  dlarngenn_cpu returns an (m x n) random double precision matrix.
 *          An uniform (0, 1) distribution is used to generate the values
 *  \param m:    (input)  Number of rows of matrix X
 *  \param n:    (input)  Number of columns of matrix X
 *  \param seed: (input)  Initial seed for the random numbers
 *  \param X:    (output) Double precision matrix (1D column major)
*/
void dlarngenn_cpu(const int m, const int n, const int seed, double *X)
{
   int iseed[4], tipo=1, tmp=(seed % 4095);
  
   if ((tmp % 2) == 0) tmp++;

   iseed[0]=iseed[1]=iseed[2]=iseed[3]=tmp;
   tmp=m*n;

   #ifdef With_MKL
     dlarnv (&tipo, iseed, &tmp, X);
   #else
     dlarnv_(&tipo, iseed, &tmp, X);
   #endif
}


/**
 *  \fn     void slarngenn_cpu(const int m, const int n, const int seed, float *X)
 *  \brief  slarngenn_cpu returns an (m x n) random simple precision matrix.
 *          An uniform (0, 1) distribution is used to generate the values
 *  \param m:    (input)  Number of rows of matrix X
 *  \param n:    (input)  Number of columns of matrix X
 *  \param seed: (input)  Initial seed for the random numbers
 *  \param X:    (output) Simple precision matrix (1D column major)
*/
void slarngenn_cpu(const int m, const int n, const int seed, float *X)
{
   int iseed[4], tipo=1, tmp=(seed % 4095);
  
   if ((tmp % 2) == 0) tmp++;

   iseed[0]=iseed[1]=iseed[2]=iseed[3]=tmp;
   tmp=m*n;

   #ifdef With_MKL
     slarnv (&tipo, iseed, &tmp, X);
   #else
     slarnv_(&tipo, iseed, &tmp, X);
   #endif
}


/**
 *  \fn     double derror_cpu(const int m, const int n, const int k, const double *A, const double *W, const double *H)
 *  \brief  derror_cpu returns double precision "2norm(A - WH) / sqrt(m x n)" 
 *  \param m: (input) Number of rows of matrix A and number of rows of W
 *  \param n: (input) Number of columns of matrix A and number of columns of H
 *  \param k: (input) Number of columns of matrix W and number of rows of H
 *  \param A: (input) Double precision matrix, dimension (m x n), 1D layout column major
 *  \param W: (input) Double precision matrix, dimension (m x k), 1D layout column major
 *  \param H: (input) Double precision matrix, dimension (k x n), 1D layout column major
*/
double derror_cpu(const int m, const int n, const int k, const double *A, const double *W, const double *H)
{
   double error=0.0, *tmp =NULL;

   tmp=(double *)nmf_alloc(m * n * sizeof *tmp, WRDLEN);

   cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, tmp, m);
   dsub_cpu   (m*n, A, tmp, tmp);

   error=cblas_dnrm2(m*n, tmp, 1);

   nmf_free(tmp);
   return (error / sqrt(m*n));
}


/**
 *  \fn     float serror_cpu(const int m, const int n, const int k, const float *A, const float *W, const float *H)
 *  \brief  serror_cpu returns simple precision "2norm(A - WH) / sqrt(m x n)" 
 *  \param m: (input) Number of rows of matrix A and number of rows of W
 *  \param n: (input) Number of columns of matrix A and number of columns of H
 *  \param k: (input) Number of columns of matrix W and number of rows of H
 *  \param A: (input) Simple precision matrix, dimension (m x n), 1D layout column major
 *  \param W: (input) Simple precision matrix, dimension (m x k), 1D layout column major
 *  \param H: (input) Simple precision matrix, dimension (k x n), 1D layout column major
*/
float serror_cpu(const int m, const int n, const int k, const float *A, const float *W, const float *H)
{
   float error=0.0, *tmp =NULL;

   tmp=(float *)nmf_alloc(m * n * sizeof *tmp, WRDLEN);

   cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, tmp, m);
   ssub_cpu   (m*n, A, tmp, tmp);

   error=cblas_snrm2(m*n, tmp, 1);

   nmf_free(tmp);
   return (error / sqrtf((float)m*n));
}


//TODO documentar
void dnrm2Cols_cpu(const int m, const int n, double *__restrict__ M)
{
   int i;
   double norm;
    
   #ifdef With_ICC
     #pragma loop_count min=32
     #pragma omp simd
   #else
     #pragma omp parallel for
   #endif
   for( i = 0; i < n; i++)
   {
      norm = cblas_dnrm2(m, &M[i*m], 1) + dEPS;
      cblas_dscal(m, 1.0/norm, &M[i*m], 1);
   }
}


//TODO documentar
void snrm2Cols_cpu(const int m, const int n, float *__restrict__ M)
{
   int i;
   float norm;
    
   #ifdef With_ICC
     #pragma loop_count min=32
     #pragma omp simd
   #else
     #pragma omp parallel for
   #endif
   for( i = 0; i < n; i++)
   {
      norm = cblas_snrm2(m, &M[i*m], 1) + sEPS;
      cblas_sscal(m, 1.0/norm, &M[i*m], 1);
   }
}


/**
 *  \fn    void dtrans(const int m, const int n, const double *M, double *__restrict__ Mt)
 *  \brief This function performs copy and transposition of a double precision matrix Mt = M'
 *  \param m:  (input) Number of rows of M
 *  \param n:  (input) Number of cols of M
 *  \param M:  (input) Double precision matrix (1D column-major)
 *  \param Mt: (output) Double precision matrix (1D column-major)
*/
void dtrans_cpu(const int m, const int n, const double *M, double *__restrict__ Mt)
{
   //#ifdef With_MKL // Ranilla 6-3-2019 probar modificacion mkl
   //  mkl_domatcopy('C', 'T', n, m, 1, M, m, Mt, n);
   //#else
     int i, j;
     #ifdef With_ICC
       #pragma loop_count min=512
       #pragma omp simd
     #else
       #pragma omp parallel for
     #endif
     for (j=0; j<n; j++)
        for (i=0; i<m; i++)
            Mt[j+i*n] = M[i+j*m];
   //#endif
}


/**
 *  \fn    void strans(const int m, const int n, const float *M, float *__restrict__ Mt)
 *  \brief This function performs copy and transposition of a double precision matrix Mt = M'
 *  \param m:  (input) Number of rows of M
 *  \param n:  (input) Number of cols of M
 *  \param M:  (input) Simple precision matrix (1D column-major)
 *  \param Mt: (output) Simple precision matrix (1D column-major)
*/
void strans_cpu(const int m, const int n, const float *M, float *__restrict__ Mt)
{
   //#ifdef With_MKL // Ranilla 6-3-2019 probar modificacion mkl
   //  mkl_somatcopy('C', 'T', n, m, 1, M, m, Mt, n);
   //#else
     int i, j;
     #ifdef With_ICC
       #pragma loop_count min=512
       #pragma omp simd
     #else
       #pragma omp parallel for
     #endif
     for (j=0; j<n; j++)
        for (i=0; i<m; i++)
            Mt[j+i*n] = M[i+j*m];
   //#endif
}


//TODO documentar
void dhalfwave_cpu(const int n, double *__restrict__ x, const int incx)
{
   int i;

   #ifdef With_ICC
     #pragma loop_count min=32
     #pragma omp simd
   #else
     #pragma omp parallel for
   #endif
   for (i=0; i<n; i++) { if(x[i*incx] < dEPS) x[i*incx] = dEPS; }
}


//TODO documentar
void shalfwave_cpu(const int n, float *__restrict__ x, const int incx)
{
   int i;

   #ifdef With_ICC
     #pragma loop_count min=32
     #pragma omp simd
   #else
     #pragma omp parallel for
   #endif
   for (i=0; i<n; i++) { if(x[i*incx] < sEPS) x[i*incx] = sEPS; }
}


// Updated by Ranilla 27/02/2019
/**
 *  \fn    int Idmin_cpu(const int n, const double *__restrict__ x)
 *  \brief This function return the position of the 1st minimum in a vector
 *  \param n: (input) Number of elements of x
 *  \param x: (input) Double precision input vector/matrix (1D column-major)
 *  \return integer index of minimum element of X
*/
int Idmin_cpu(const int n, const double *__restrict__ x)
{
   #ifdef OMP
     int i;

     #ifdef OMP4
       dPosMin data ={DBL_MAX, 0};
       
       #pragma omp parallel for reduction(dMIN: data)
       for (i=0; i<n; i++)
         if (x[i] < data.val) { data.val=x[i]; data.pos=i; }
                         
       return data.pos;
     #else
       int    minIdx = 0;
       double minVal = x[0];

       #pragma omp parallel
       {
         int minIdxLocal = minIdx;
         double minLocal = minVal;

         #pragma omp for nowait
           for(i=1; i<n; i++)
             if(x[i] < minLocal) { minLocal=x[i]; minIdxLocal=i; }
			
         #pragma omp critical
         {
           if(minLocal < minVal) { minVal = minLocal; minIdx = minIdxLocal; }
           else if ((minLocal == minVal) && (minIdxLocal < minIdx)) minIdx = minIdxLocal;
         }
       }
       return minIdx;
     #endif
   #else
     int i, minIdx=0;
     double minVal=x[0];

     for(i=1; i<n; i++)
       if (x[i] < minVal) { minVal=x[i]; minIdx=i; }

     return minIdx;
   #endif
}


// Updated by Ranilla 27/02/2019
/**
 *  \fn    int Ismin_cpu(const int n, const float *__restrict__ x)
 *  \brief This function return the position of the 1st minimum in a vector
 *  \param n: (input) Number of elements of x
 *  \param x: (input) Simple precision input vector/matrix (1D column-major)
 *  \return integer index of minimum element of X
*/
int Ismin_cpu(const int n, const float *__restrict__ x)
{
   #ifdef OMP
     int i;

     #ifdef OMP4
       sPosMin data ={FLT_MAX, 0};
       
       #pragma omp parallel for reduction(sMIN: data)
       for (i=0; i<n; i++)
         if (x[i] < data.val) { data.val=x[i]; data.pos=i; }
                         
       return data.pos;
     #else
       int   minIdx = 0;
       float minVal = x[0];

       #pragma omp parallel
       {
         int minIdxLocal = minIdx;
         float  minLocal = minVal;

         #pragma omp for nowait
           for(i=1; i<n; i++)
             if(x[i] < minLocal) { minLocal=x[i]; minIdxLocal=i; }
			
         #pragma omp critical
         {
           if(minLocal < minVal) { minVal = minLocal; minIdx = minIdxLocal; }
           else if ((minLocal == minVal) && (minIdxLocal < minIdx)) minIdx = minIdxLocal;
         }
       }
       return minIdx;
     #endif
   #else
     int i, minIdx=0;
     float  minVal=x[0];

     for(i=1; i<n; i++)
       if (x[i] < minVal) { minVal=x[i]; minIdx=i; }

     return minIdx;
   #endif
}


// New by Ranilla 27/02/2019
/**
 *  \fn    int Idamin_cpu(const int n, const double *__restrict__ x)
 *  \brief This function finds the index of 1st element having the min. absolute value, in parallel
 *  \param n: (input) Number of elements of x
 *  \param x: (input) Double precision input vector/matrix (1D column-major)
 *  \return integer index of 1st element having the min. absolute value of x
*/
int Idamin_cpu(const int n, const double *__restrict__ x)
{
   #ifdef OMP
     int i;
   
     #ifdef OMP4
       dPosMin data ={DBL_MAX, 0};
       
       #pragma omp parallel for reduction(dMIN: data)
       for (i=0; i<n; i++)
         if (fabs(x[i]) < data.val) { data.val=fabs(x[i]); data.pos=i; }
                         
       return data.pos;
     #else
       int    minIdx = 0;
       double minVal = fabs(x[0]);

       #pragma omp parallel
       {
         int minIdxLocal = minIdx;
         double minLocal = minVal;

         #pragma omp for nowait
           for(i=1; i<n; i++)
             if(fabs(x[i]) < minLocal) { minLocal=fab(xs[i]); minIdxLocal=i; }
			
         #pragma omp critical
         {
           if(minLocal < minVal) { minVal = minLocal; minIdx = minIdxLocal; }
           else if ((minLocal == minVal) && (minIdxLocal < minIdx)) minIdx = minIdxLocal;
         }
       }
       return minIdx;
     #endif
   #else
     int i, minIdx=0;
     double minVal=fabs(x[0]);

     for(i=1; i<n; i++)
       if (fabs(x[i]) < minVal) { minVal=fabs(x[i]); minIdx=i; }

     return minIdx;
   #endif
}


// New by Ranilla 27/02/2019
/**
 *  \fn    int Isamin_cpu(const int n, const float *__restrict__ x)
 *  \brief This function finds the index of 1st element having the min. absolute value, in parallel
 *  \param n: (input) Number of elements of x
 *  \param x: (input) Simple precision input vector/matrix (1D column-major)
 *  \return integer index of 1st element having the min. absolute value of x
*/
int Isamin_cpu(const int n, const float *__restrict__ x)
{
   #ifdef OMP
     int i;
   
     #ifdef OMP4
       sPosMin data ={FLT_MAX, 0};
       
       #pragma omp parallel for reduction(sMIN: data)
       for (i=0; i<n; i++)
         if (fabsf(x[i]) < data.val) { data.val=fabsf(x[i]); data.pos=i; }
                         
       return data.pos;
     #else
       int   minIdx = 0;
       float minVal = fabsf(x[0]);

       #pragma omp parallel
       {
         int minIdxLocal = minIdx;
         float  minLocal = minVal;

         #pragma omp for nowait
           for(i=1; i<n; i++)
             if(fabsf(x[i]) < minLocal) { minLocal=fab(xs[i]); minIdxLocal=i; }
			
         #pragma omp critical
         {
           if(minLocal < minVal) { minVal = minLocal; minIdx = minIdxLocal; }
           else if ((minLocal == minVal) && (minIdxLocal < minIdx)) minIdx = minIdxLocal;
         }
       }
       return minIdx;
     #endif
   #else
     int i, minIdx=0;
     float  minVal=fabsf(x[0]);

     for(i=1; i<n; i++)
       if (fabsf(x[i]) < minVal) { minVal=fabsf(x[i]); minIdx=i; }

     return minIdx;
   #endif
}
