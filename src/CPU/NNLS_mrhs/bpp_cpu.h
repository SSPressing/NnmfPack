/**
 *  \file    bpp_cpu.h
 *  \brief   Header file for using the BPP algorithm to solve the NNLS problem with multiple rigth hand sides  with CPU
 *  \author  P. San Juan
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \date    08/05/18
*/
#ifndef BPP_CPU_H
#define BPP_CPU_H

#include <utils_cpu.h>
#include <string.h>
#include "nnls_cpu.h" //Public interface of the library


/*support functions*/
void dextractSubmatricesCtC(const int, const double *, int *, const int, double *, double *);
void sextractSubmatricesCtC(const int, const float *,  int *, const int, float *,  float  *);

void dextractSubmatricesCtB(const int, const double *, int *, const int, const int *, const int, double *, double *);
void sextractSubmatricesCtB(const int, const float *,  int *, const int, const int *, const int, float  *, float *);

void addIdxs   (int *, int *, const int *, const int);
void removeIdxs(int *, int *, const int *, const int);

int setEquals(const int *, const int, const int *, const int);
#endif
