/**
 *  \file    asna_cpu.h
 *  \brief   Header file for using the ASNA algorithm to solve the NNLS problem with multiple rigth hand sides using the Kullback-Leibler divergence as error measure with CPU
 *  \author  P. San Juan
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \date    08/05/18
*/
#ifndef ASNA_CPU_H
#define ASNA_CPU_H

#include <utils_cpu.h>
#include "nnls_cpu.h" //Public interface of the library

#define dTOL dEPS*10 // dEPS*10; dEPS is 1e-16
#define sTOL sEPS*10 // sEPS*10; sEPS is 1e-08

#define dGRA 1e-10
#define sGRA 1e-05

/*support functions*/
typedef struct atom atom;
typedef struct observation observation;

//Structure to keep each obvservation information
struct observation
{
   int obsIdx; 
   int converged;   // 0 if the observation is not converged
   int nAtoms;      //number of active atoms in the observation
   atom *firstAtom; //First element in the double linked list of active atoms
   atom *lastAtom;  //First element in the double linked list of active atoms
};

//Element of double linked list of atoms
struct atom
{
   int atomIdx; //Position of atom in the observation
   atom *prevAtom;
   atom *nextAtom;
};

void addAtom   (observation *, const int);
void removeAtom(observation *, const int);

void freeObservations(int, observation *);

void dmultOnes(const int, const int, const int, const double *, double *__restrict__);
void smultOnes(const int, const int, const int, const float *,  float *__restrict__);

int dInitAtoms(const int, const int, const int, const double *, const double *, int *__restrict__, double *__restrict__);
int sInitAtoms(const int, const int, const int, const float *,  const float *,  int *__restrict__, float *__restrict__);

double derrorAsna(const int, const int, const int, const double *, const double *, const double *);
float  serrorAsna(const int, const int, const int, const float *,  const float *,  const float *);

void dnrm1ColsSave(const int, const int, double *__restrict__, double *__restrict__); 
void snrm1ColsSave(const int, const int, float *__restrict__,  float *__restrict__); 

void dnrm2ColsSave(const int, const int, double *__restrict__, double *__restrict__); 
void snrm2ColsSave(const int, const int, float *__restrict__,  float *__restrict__); 

void dcolsRescale(const int, const int, double *__restrict__, const double *__restrict__); 
void scolsRescale(const int, const int, float *__restrict__,  const float *__restrict__); 

void drowsRescale(const int, const int, double *__restrict__, const double *__restrict__); 
void srowsRescale(const int, const int, float *__restrict__,  const float *__restrict__); 

void daddEsc_cpu(const int, const double, double *__restrict__);
void saddEsc_cpu(const int, const float , float *__restrict__);

void derrorbd1_cpu(const int n, const double *, double *__restrict__);
void serrorbd1_cpu(const int n, const float *,  float *__restrict__);

#endif
