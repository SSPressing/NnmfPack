/***************************************************************************
 *   Copyright (C) 2014 by PIR (University of Oviedo) and                  *
 *   INCO2 (Polytechnic University of Valencia) groups.                    *
 *    nmfpack@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************
*/
/**
 *  \file    pbdiv_cpu.c
 *  \brief   File with functions to calcule  partial NNMF decompositions using betadivergence methods for CPUs
 *  \author  Information Retrieval and Parallel Computing Group (IRPCG)
 *  \author  University of Oviedo, Spain
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \author  Contact: nmfpack@gmail.com  
 *  \date    08/05/18
*/

#include "pbdiv_cpu.h"


/**
 *  \fn    int dpbdivg_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const double expo, const int uType, const int nIter)
 *  \brief dpbdivg_cpu performs the NNMF using beta-divergence when beta is != 1 and !=2, using double precision.
 *
 *         The algorithm is<BR>
 *         &nbsp;&nbsp;repit nIter times<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;STEP 1<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L=W*H<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L1=L(.^)(beta-2)<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L2=L1(.*)A<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L1=L1(.*)L<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B=W'*L2<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C=W'*L1<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;H=H(.*)B(./)C<BR>
 *
 *         &nbsp;&nbsp;&nbsp;&nbsp;STEP 2<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L=W*H<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L1=L(.^)(beta-2)<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L2=L1(.*)A<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L1=L1(.*)L<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D=L2*H'<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E=L1*H'<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;W=W(.*)D(./)E<BR>
 *         &nbsp;&nbsp;end repit<BR>
 *         End algorithm<BR>
 *
 *         In real live L1 and L2 are (m*n) matrices used in STEP 1 and 2. For performance
 *         reasons only one 1D colum-mayor buffer, named R in next code, of size 2*m*n is used
 *         In STEP 1, first part of R (m*n positions) is L2 and the second part is L1.
 *         In STEP 2, fisrt column of R (2*m positions) is composed by the first column of L2
 *         following first column of L1. Second column of R (2*m positions) is composed by the
 *         sencond column of L2 following second column of L1. 3rd column of R ... and so on
 *
 *         In real live B and C are (k*n) matrices used in STEP 1, and D and E are (m*k)
 *         matrices used in STEP 2. B/C and D/E are independent. However we do not have L1
 *         and L2, we have R, and we can do B=W'*L2 and C=W'*L1 (or D=L2*H' and E=L1*H') at
 *         the same time. For this reason only one matrix is declared to save space. This is
 *         matrix M with size 2*max(m,n)*k
 *
 *  \param m:     (input) Number of rows of matrix A and matrix W
 *  \param n:     (input) Number of columns of matrix A and matrix H
 *  \param k:     (input) Number of columns of matrix W and rows of H
 *  \param A:     (input) Double precision input matrix of (m * n) number of elements stored using 1D column-major
 *  \param W:     (inout) Double precision input/output matrix of (m * k) number of elements stored using 1D column-major
 *  \param H:     (inout) Double precision input/output matrix of (k * n) number of elements stored using 1D column-major
 *  \param expo:  (input) Double precision value. The exponent beta of betadivergence method
 *  \param uType: (input) It can be UpdateAll (W and H are updated), UpdateW (only H is updated) or UpdateH (only W is updated)
 *  \param nIter: (input) Number of iterations
 *
 *  It returns 0 if all is OK.
*/
int dpbdivg_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const double expo, const int uType, int nIter)
{
   double *L=NULL, *M=NULL, *R=NULL;
   int     i;

   switch (uType)
   {
     case UpdateAll:
        dbdivg_cpu(m, n, k, A, W, H, expo, nIter);
        break;

     case UpdateW:
        M = (double *)nmf_alloc(2*m * k * sizeof *M, WRDLEN);
        L = (double *)nmf_alloc(  m * n * sizeof *L, WRDLEN);
        R = (double *)nmf_alloc(2*m * n * sizeof *R, WRDLEN);

        if (L == NULL || M == NULL || R == NULL) { return -1; }

        for(i=0; i<nIter; i++)
        {
          /* ************************ Phase 2 *************************** */
          /* L=W*H */
          cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);     	

          /* L1=L(.^)(expo-2) */
          /* L2=L1(.*)A       */
          /* L1=L1(.*)L       */
          /* R is L1 and L2   */
          dkernelW_cpu(m, n , L, A, R, expo-2.0);   

          /* D=L2*H' */
          /* E=L1*H' */
          /*                     |L2|      */
          /* above is equal to R=|  | * H' */
          /*                     |L1|      */   
          cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans, 2*m, k, n, 1.0, R, 2*m, H, k, 0.0, M, 2*m);

          /* W=W(.*)D(./)E */
         dupdate1W_cpu(m, k, M, W);
        }
        nmf_free(L);
        nmf_free(R);
        nmf_free(M);
        break;

     case UpdateH:
        M = (double *)nmf_alloc(2*n * k * sizeof *M, WRDLEN);
        L = (double *)nmf_alloc(  m * n * sizeof *L, WRDLEN);
        R = (double *)nmf_alloc(2*m * n * sizeof *R, WRDLEN);

        if (L == NULL || M == NULL || R == NULL) { return -1; }

        for(i=0; i<nIter; i++)
        {
          /* ************************ Phase 1 *************************** */
          /* L=W*H */
          cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);     		    

          /* L1=L(.^)(expo-2) */
          /* L2=L1(.*)A       */
          /* L1=L1(.*)L       */
          /* R is L1 and L2   */
          dkernelH_cpu(m, n, L, A, R, expo-2.0);

          /* B=W'*L2 */
          /* C=W'*L1 */
          /* above is equal to R=W'*|L2 | L1| */
          cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, 2*n, m, 1.0, W, m, R, m, 0.0, M, k);

          /* H=H(.*)B(./)C. Note that matrices B and C are stored together in matrix M*/
          dupdate1H_cpu(k*n, M, H);
        }
        nmf_free(L);
        nmf_free(R);
        nmf_free(M);
        break;

     default:
        return -1;
  } 
  return 0;
}


/**
 *  \fn    int spbdivg_cpu(const int m, const int n, const int k, const float *A, float *W, float *H, const float expo, const int uType, const int nIter)
 *  \brief spbdivg_cpu performs NNMF using betadivergence for general case (beta <> 1 and 2) using simple precision
 *  \param m:     (input) Number of rows of matrix A and matrix W
 *  \param n:     (input) Number of columns of matrix A and matrix H
 *  \param k:     (input) Number of columns of matrix W and rows of H
 *  \param A:     (input) Simple precision input matrix of (m * n) number of elements stored using 1D column-major
 *  \param W:     (inout) Simple precision input/output matrix of (m * k) number of elements stored using 1D column-major
 *  \param H:     (inout) Simple precision input/output matrix of (k * n) number of elements stored using 1D column-major
 *  \param expo:  (input) Simple precision value. The exponent beta of betadivergence method
 *  \param uType: (input) It can be UpdateAll (W and H are updated), UpdateW (only H is updated) or UpdateH (only W is updated)
 *  \param nIter: (input) Number of iterations
 *  \return: 0 if all is OK, -1 otherwise 
*/
int spbdivg_cpu(const int m, const int n, const int k, const float *A, float *W, float *H, const float expo, const int uType, int nIter)
{
   float *L=NULL, *M=NULL, *R=NULL;
   int    i;

   switch (uType)
   {
     case UpdateAll:
        sbdivg_cpu(m, n, k, A, W, H, expo, nIter);
        break;

     case UpdateW:
        M = (float *)nmf_alloc(2*m * k * sizeof *M, WRDLEN);
        L = (float *)nmf_alloc(  m * n * sizeof *L, WRDLEN);
        R = (float *)nmf_alloc(2*m * n * sizeof *R, WRDLEN);

        if (L == NULL || M == NULL || R == NULL) { return -1; }

        for(i=0;i<nIter;i++)
        {
           /* ************************ Phase 2 *************************** */
           /* L=W*H */
           cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);     	

           /* L1=L(.^)(expo-2) */
           /* L2=L1(.*)A       */
           /* L1=L1(.*)L       */
           /* R is L1 and L2   */
           skernelW_cpu(m, n, L, A, R, expo-2.0f);

           /* D=L2*H' */
           /* E=L1*H' */
           /*                     |L2|      */
           /* above is equal to R=|  | * H' */
           /*                     |L1|      */   
           cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans, 2*m, k, n, 1.0, R, 2*m, H, k, 0.0, M, 2*m);

           /* W=W(.*)D(./)E */
           supdate1W_cpu(m, k, M, W);
        }
        nmf_free(L);
        nmf_free(R);
        nmf_free(M);
        break;

     case UpdateH:
        M = (float *)nmf_alloc(2*n * k * sizeof *M, WRDLEN);
        L = (float *)nmf_alloc(  m * n * sizeof *L, WRDLEN);
        R = (float *)nmf_alloc(2*m * n * sizeof *R, WRDLEN);

        if (L == NULL || M == NULL || R == NULL) { return -1; }

        for(i=0;i<nIter;i++)
        {
           /* ************************ Phase 1 *************************** */
           /* L=W*H */
           cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);     		    

           /* L1=L(.^)(expo-2) */
           /* L2=L1(.*)A       */
           /* L1=L1(.*)L       */
           /* R is L1 and L2   */
           skernelH_cpu(m, n, L, A, R, expo-2.0f);

           /* B=W'*L2 */
           /* C=W'*L1 */
           /* above is equal to R=W'*|L2 | L1| */
           cblas_sgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, 2*n, m, 1.0, W, m, R, m, 0.0, M, k);

           /* H=H(.*)B(./)C. Note that matrices B and C are stored together in matrix M*/
           supdate1H_cpu(k*n, M, H);
        }
        nmf_free(L);
        nmf_free(R);
        nmf_free(M);
        break;

     default:
        return -1;
  } 
  return 0;
}


/** TODO documentar
 * 
 */
int dpbdivgRestrict_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const double expo,
                        const int uType, const int nIter, const double alphaW, const double alphaH, const unsigned short restr)
{
   double *L=NULL, *M=NULL, *R=NULL;	
   int     i;

   if ((alphaW == 0 && alphaH == 0) || restr == 0)
      return dpbdivg_cpu(m, n, k, A, W, H, expo, UpdateAll, nIter);

   if(restr > 4) { return -2; }

   switch (uType)
   {
     case UpdateAll:
        dbdivgRestrict_cpu(m, n, k, A, W, H, expo, nIter, alphaW, alphaH, restr);
        break;

     case UpdateW:
        M = (double *)nmf_alloc(2*m * k * sizeof *M, WRDLEN);
        L = (double *)nmf_alloc(  m * n * sizeof *L, WRDLEN);
        R = (double *)nmf_alloc(2*m * n * sizeof *R, WRDLEN);

        if (L == NULL || M == NULL || R == NULL) { return -1; }

        for(i=0;i<nIter;i++)
        {
          /* ************************ Phase 2 *************************** */
          /* L=W*H */
          cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);     	

          /* L1=L(.^)(expo-2) */
          /* L2=L1(.*)A       */
          /* L1=L1(.*)L       */
          /* R is L1 and L2   */
          dkernelW_cpu(m, n , L, A, R, expo-2.0);   

          /* D=L2*H' */
          /* E=L1*H' */
          /*                     |L2|      */
          /* above is equal to R=|  | * H' */
          /*                     |L1|      */   
          cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans, 2*m, k, n, 1.0, R, 2*m, H, k, 0.0, M, 2*m);

          switch (restr)
          {
            case 1:
            case 3:
               drestr1normSub_cpu(m, k, alphaW, M, 2*m);
               break;
            case 2:
            case 4:
               drestr2normSub_cpu(m, k, alphaW, W, m, M, 2*m);
               break;
          }

          /* W=W(.*)D(./)E */
          dupdate1W_cpu(m, k, M, W);
        }
        nmf_free(L);
        nmf_free(R);
        nmf_free(M);
        break;

     case UpdateH:
        M = (double *)nmf_alloc(2*n * k * sizeof *M, WRDLEN);
        L = (double *)nmf_alloc(  m * n * sizeof *L, WRDLEN);
        R = (double *)nmf_alloc(2*m * n * sizeof *R, WRDLEN);

        if (L == NULL || M == NULL || R == NULL) { return -1; }

        for(i=0;i<nIter;i++)
        {
          /* ************************ Phase 1 *************************** */
          /* L=W*H */
          cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);     		    

          /* L1=L(.^)(expo-2) */
          /* L2=L1(.*)A       */
          /* L1=L1(.*)L       */
          /* R is L1 and L2   */
          dkernelH_cpu(m, n, L, A, R, expo-2.0);

          /* B=W'*L2 */
          /* C=W'*L1 */
          /* above is equal to R=W'*|L2 | L1| */
          cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, 2*n, m, 1.0, W, m, R, m, 0.0, M, k);

          switch (restr)
          {
            case 1:
            case 4:
               drestr1norm_cpu(k*n, alphaH, M);
               break;
            case 2:
            case 3:
               drestr2norm_cpu(k*n, alphaH, H, M);
               break;
          }

          /* H=H(.*)B(./)C. Note that matrices B and C are stored together in matrix M*/
          dupdate1H_cpu(k*n, M, H);
        }
        nmf_free(L);
        nmf_free(R);
        nmf_free(M);
        break;

     default:
        return -1;
  } 
  return 0;
}


/** TODO documentar 
 * 
 */ 
int spbdivgRestrict_cpu(const int m, const int n, const int k, const float *A, float *W, float *H, const float expo, const int uType,
                       const int nIter, const float alphaW, const float alphaH, const unsigned short restr)
{
   float *L=NULL, *M=NULL, *R=NULL;
   int    i;

   if ((alphaW == 0 && alphaH == 0) || restr == 0)
      return spbdivg_cpu(m, n, k, A, W, H, expo, UpdateAll, nIter);

   if(restr > 4) { return -2; }

   switch (uType)
   {
     case UpdateAll:
        sbdivgRestrict_cpu(m, n, k, A, W, H, expo, nIter, alphaW, alphaH, restr);
        break;

     case UpdateW:
        M = (float *)nmf_alloc(2*max(m,n)* k * sizeof *M, WRDLEN);
        L = (float *)nmf_alloc(        m * n * sizeof *L, WRDLEN);
        R = (float *)nmf_alloc(      2*m * n * sizeof *R, WRDLEN);

        if (L == NULL || M == NULL || R == NULL) { return -1; }

        for(i=0;i<nIter;i++)
        {
          /* ************************ Phase 2 *************************** */
          /* L=W*H */
          cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);     	

          /* L1=L(.^)(expo-2) */
          /* L2=L1(.*)A       */
          /* L1=L1(.*)L       */
          /* R is L1 and L2   */
          skernelW_cpu(m, n , L, A, R, expo-2.0f);   

          /* D=L2*H' */
          /* E=L1*H' */
          /*                     |L2|      */
          /* above is equal to R=|  | * H' */
          /*                     |L1|      */   
          cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans, 2*m, k, n, 1.0, R, 2*m, H, k, 0.0, M, 2*m);
           
          switch (restr)
          {
             case 1:
             case 3:
                srestr1normSub_cpu(m, k, alphaW, M, 2*m);
                break;
             case 2:
             case 4:
                srestr2normSub_cpu(m, k, alphaW, W, m, M, 2*m);
                break;
          }
            
          /* W=W(.*)D(./)E */
          supdate1W_cpu(m, k, M, W);
        }
        nmf_free(L);
        nmf_free(R);
        nmf_free(M);
        break;

     case UpdateH:
        M = (float *)nmf_alloc(2*max(m,n)* k * sizeof *M, WRDLEN);
        L = (float *)nmf_alloc(        m * n * sizeof *L, WRDLEN);
        R = (float *)nmf_alloc(      2*m * n * sizeof *R, WRDLEN);

        if (L == NULL || M == NULL || R == NULL) { return -1; }

        for(i=0;i<nIter;i++)
        {
          /* ************************ Phase 1 *************************** */
          /* L=W*H */
          cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);     		    

          /* L1=L(.^)(expo-2) */
          /* L2=L1(.*)A       */
          /* L1=L1(.*)L       */
          /* R is L1 and L2   */
          skernelH_cpu(m, n, L, A, R, expo-2.0f);

          /* B=W'*L2 */
          /* C=W'*L1 */
          /* above is equal to R=W'*|L2 | L1| */
          cblas_sgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, 2*n, m, 1.0, W, m, R, m, 0.0, M, k);

          switch (restr)
          {
             case 1:
             case 4:
                srestr1norm_cpu(k*n, alphaH, M);
                break;
             case 2:
             case 3:
                srestr2norm_cpu(k*n, alphaH, H, M);
                break;
          }

          /* H=H(.*)B(./)C. Note that matrices B and C are stored together in matrix M*/
          supdate1H_cpu(k*n, M, H);
        }
        nmf_free(L);
        nmf_free(R);
        nmf_free(M);
        break;

     default:
        return -1;
  } 
  return 0;
}


/**
 *  \fn    int dpmlsa_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const int uType, const int nIter)
 *  \brief dpmlsa_cpu performs NNMF using betadivergence when beta=2 using double precision
 *
 *         The algorithm is<BR>
 *         &nbsp;&nbsp;repit nIter times<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;STEP 1<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L=W*H<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B=W'*A<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C=W'*L<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;H=H(.*)B(./)C<BR>
 *
 *         &nbsp;&nbsp;&nbsp;&nbsp;STEP 2<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L=W*H<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D=A*H'<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E=L*H'<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;W=W(.*)D(./)E<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;end repit<BR>
 *         End algorithm<BR>
 *
 *         To save some FLOPs and RAM a modified Lee and Seung version is used, so we have Step 1: L=W'*W,
 *         C=L*H and then B=W'*A; Step 2: L=H*H', E=W*L and D=A*H'. W'*W (H*H') and L*H (W*L) can do in
 *         parallel with W'*A (A*H'). Because dgemm works fine we don't use threads to do in parallel.
 *
 *         In real live B and C are (k*n) matrices used in STEP 1, and D and E are (m*k)
 *         matrices used in STEP 2. B/C and D/E are independent. For this reason only 2 matrices are declared
 *         to save space. They are matrices B and C with size max(m,n)*k
 *
 *  \param m:     (input) Number of rows of matrix A and matrix W
 *  \param n:     (input) Number of columns of matrix A and matrix H
 *  \param k:     (input) Number of columns of matrix W and rows of H
 *  \param A:     (input) Double precision input matrix of (m * n) number of elements stored using 1D column-major
 *  \param W:     (inout) Double precision input/output matrix of (m * k) number of elements stored using 1D column-major
 *  \param H:     (inout) Double precision input/output matrix of (k * n) number of elements stored using 1D column-major
 *  \param uType: (input) It can be UpdateAll (W and H are updated), UpdateW (only H is updated) or UpdateH (only W is updated)
 *  \param nIter: (input) Number of iterations
 *  \return: 0 if all is OK, -1 otherwise 
*/
int dpmlsa_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const int uType, const int nIter)
{
   double *B=NULL, *C=NULL, *L=NULL;
   int    i;

   switch (uType)
   {
     case UpdateAll:
        dmlsa_cpu(m, n, k, A, W, H, nIter);
        break;

     case UpdateW:
        B = (double *)nmf_alloc(m * k * sizeof *B, WRDLEN);
        C = (double *)nmf_alloc(m * k * sizeof *C, WRDLEN);
        L = (double *)nmf_alloc(k * k * sizeof *L, WRDLEN);

        if (B == NULL || C == NULL || L == NULL) { return -1; }

        for(i=0;i<nIter;i++)
        {
           /* ************************ Phase 2 *************************** */
           /* B=A*H' */
           cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans,   m, k, n, 1.0, A, m, H, k, 0.0, B, m);

           /* L=H*H' */
           cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans,   k, k, n, 1.0, H, k, H, k, 0.0, L, k);

           /* C=W*L */
           cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, k, k, 1.0, W, m, L, k, 0.0, C, m);

           /* W=W(*)B(/)C */
           ddotdiv_cpu(m*k, B, C, W);
        }
        nmf_free(B);
        nmf_free(C);
        nmf_free(L);
        break;

     case UpdateH:
        B = (double *)nmf_alloc(n * k * sizeof *B, WRDLEN);
        C = (double *)nmf_alloc(n * k * sizeof *C, WRDLEN);
        L = (double *)nmf_alloc(k * k * sizeof *L, WRDLEN);
	
        if (B == NULL || C == NULL || L == NULL) { return -1; }

        for(i=0;i<nIter;i++)
        {
           /* ************************ Phase 1 *************************** */
           /* B=W'*A */	
           cblas_dgemm(CblasColMajor, CblasTrans,   CblasNoTrans, k, n, m, 1.0, W, m, A, m, 0.0, B, k);		

           /* L=W'*W */
           cblas_dgemm(CblasColMajor, CblasTrans,   CblasNoTrans, k, k, m, 1.0, W, m, W, m, 0.0, L, k);

           /* C=L*H */
           cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, k, n, k, 1.0, L, k, H, k, 0.0, C, k);

           /* H=H(*)B(/)C */
           ddotdiv_cpu(k*n, B, C, H);
        }
        nmf_free(B);
        nmf_free(C);
        nmf_free(L);
        break;

     default:
        return -1;
  } 
  return 0;
}


/**
 *  \fn    int smlsa_cpu(const int m, const int n, const int k, const float *A, float *W, float *H, const int uType, const int nIter)
 *  \brief smlsa_cpu performs NNMF using betadivergence when beta=2 using simple precision
 *  \param m:     (input) Number of rows of matrix A and matrix W
 *  \param n:     (input) Number of columns of matrix A and matrix H
 *  \param k:     (input) Number of columns of matrix W and rows of H
 *  \param A:     (input) Simple precision input matrix of (m * n) number of elements stored using 1D column-major
 *  \param W:     (inout) Simple precision input/output matrix of (m * k) number of elements stored using 1D column-major
 *  \param H:     (inout) Simple precision input/output matrix of (k * n) number of elements stored using 1D column-major
 *  \param uType: (input) It can be UpdateAll (W and H are updated), UpdateW (only H is updated) or UpdateH (only W is updated)
 *  \param nIter: (input) Number of iterations
 *  \return: 0 if all is OK, -1 otherwise  
*/
int spmlsa_cpu(const int m, const int n, const int k, const float *A, float *W, float *H, const int uType, const int nIter)
{
   float *B=NULL, *C=NULL, *L=NULL;
   int    i;

   switch (uType)
   {
     case UpdateAll:
        smlsa_cpu(m, n, k, A, W, H, nIter);
        break;

     case UpdateW:
        B = (float *)nmf_alloc(m * k * sizeof *B, WRDLEN);
        C = (float *)nmf_alloc(m * k * sizeof *C, WRDLEN);
        L = (float *)nmf_alloc(m * n * sizeof *L, WRDLEN);

        if (B == NULL || C == NULL || L == NULL) { return -1; }

        for(i=0;i<nIter;i++)
        {
           /* ************************ Phase 2 *************************** */
           /* B=A*H' */
           cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans,   m, k, n, 1.0, A, m, H, k, 0.0, B, m);

           /* L=H*H' */
           cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans,   k, k, n, 1.0, H, k, H, k, 0.0, L, k);

           /* C=W*L */
           cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, k, k, 1.0, W, m, L, k, 0.0, C, m);

           /* W=W(*)B(/)C */
           sdotdiv_cpu(m*k, B, C, W);
        }
        nmf_free(B);
        nmf_free(C);
        nmf_free(L);
        break;

     case UpdateH:
        B = (float *)nmf_alloc(n * k * sizeof *B, WRDLEN);
        C = (float *)nmf_alloc(n * k * sizeof *C, WRDLEN);
        L = (float *)nmf_alloc(m * n * sizeof *L, WRDLEN);

        if (B == NULL || C == NULL || L == NULL) { return -1; }

        for(i=0;i<nIter;i++)
        {
           /* ************************ Phase 1 *************************** */
           /* B=W'*A */	
           cblas_sgemm(CblasColMajor, CblasTrans,   CblasNoTrans, k, n, m, 1.0, W, m, A, m, 0.0, B, k);

           /* L=W'*W */
           cblas_sgemm(CblasColMajor, CblasTrans,   CblasNoTrans, k, k, m, 1.0, W, m, W, m, 0.0, L, k);

           /* C=L*H */
           cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, k, n, k, 1.0, L, k, H, k, 0.0, C, k);

           /* H=H(*)B(/)C */
           sdotdiv_cpu(k*n, B, C, H);
        }
        nmf_free(B);
        nmf_free(C);
        nmf_free(L);
        break;

     default:
        return -1;
  } 
  return 0;
}


/** TODO añadir comentario
 * restr. 0: nada, 1: WH 1norma, 2: WH 2norma, 3: W 1norma H 2norma, 4: W 2norma H 1norma 
 */
int dpmlsaRestrict_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const int uType,
                       const int nIter, const double alphaW, const double alphaH, const unsigned short restr)
{
   double *N=NULL, *D=NULL, *Aux=NULL;
   int     i;
  
   if ((alphaW == 0 && alphaH == 0) || restr == 0)
      return dpmlsa_cpu(m, n, k, A, W, H, UpdateAll, nIter);

   if(restr > 4) { return -2; }
    
   switch (uType)
   {
     case UpdateAll:
        dmlsaRestrict_cpu(m, n, k, A, W, H, nIter, alphaW, alphaH, restr);
        break;

     case UpdateW:
        N   = (double *)nmf_alloc(m * k * sizeof *N,   WRDLEN);
        D   = (double *)nmf_alloc(n * k * sizeof *D,   WRDLEN);
        Aux = (double *)nmf_alloc(k * k * sizeof *Aux, WRDLEN);
	
        if (N == NULL || D == NULL || Aux == NULL) { return -1; }

        for(i=0;i<nIter;i++)
        {
           /* ************************ Phase 2 *************************** */
           /* N=A*H' */
           cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans,   m, k, n, 1.0, A, m, H, k, 0.0, N, m);
            
           switch (restr)
           {
              case 1:
              case 3:
                 drestr1norm_cpu(m*k, alphaW, N);
                 break;
              case 2:
              case 4:
                 drestr2norm_cpu(m*k, alphaW, W, N);
                 break;
           }
            
           /* Aux=H*H' */
           cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans,   k, k, n, 1.0, H, k, H, k, 0.0, Aux, k);

           /* D=W*Aux */
           cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, k, k, 1.0, W, m, Aux, k, 0.0, D, m);

           /* W=W(*)N(/)D */
           ddotdiv_cpu(m*k, N, D, W);
        }
        nmf_free(N);
        nmf_free(D);
        nmf_free(Aux);
        break;

     case UpdateH:
        N   = (double *)nmf_alloc(n * k * sizeof *N,   WRDLEN);
        D   = (double *)nmf_alloc(n * k * sizeof *D,   WRDLEN);
        Aux = (double *)nmf_alloc(k * k * sizeof *Aux, WRDLEN);
	
        if (N == NULL || D == NULL || Aux == NULL) { return -1; }

        for(i=0;i<nIter;i++)
        {
           /* ************************ Phase 1 *************************** */
           /* N=W'*A */	
           cblas_dgemm(CblasColMajor, CblasTrans,   CblasNoTrans, k, n, m, 1.0, W, m, A, m, 0.0, N, k);		
            
           switch (restr)
           {
              case 1:
              case 4:
                 drestr1norm_cpu(k*n, alphaH, N);
                 break;
              case 2:
              case 3:
                 drestr2norm_cpu(k*n, alphaH, H, N);
                 break;
           }
            
           /* Aux=W'*W */
           cblas_dgemm(CblasColMajor, CblasTrans,   CblasNoTrans, k, k, m, 1.0, W, m, W, m, 0.0, Aux, k);

           /* D=Aux*H */
           cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, k, n, k, 1.0, Aux, k, H, k, 0.0, D, k);

           /* H=H(*)N(/)D */
           ddotdiv_cpu(k*n, N, D, H);
        }
        nmf_free(N);
        nmf_free(D);
        nmf_free(Aux);
        break;

     default:
        return -1;
  } 
  return 0;
}


/** TODO añadir comentario
 * restr. 0: nada, 1: WH 1norma, 2: WH 2norma, 3: W 1norma H 2norma, 4: W 2norma H 1norma 
 */
int spmlsaRestrict_cpu(const int m, const int n, const int k, const float *A, float *W, float *H, const int uType,
                       const int nIter, const float alphaW, const float alphaH, const unsigned short restr )
{
   float *N=NULL, *D=NULL, *Aux=NULL;
   int    i;
  
   if ((alphaW == 0 && alphaH == 0) || restr == 0)
      return spmlsa_cpu(m, n, k, A, W, H, UpdateAll, nIter);

   if(restr > 4) { return -2; }
    
   switch (uType)
   {
     case UpdateAll:
        smlsaRestrict_cpu(m, n, k, A, W, H, nIter, alphaW, alphaH, restr);
        break;

     case UpdateW:
        N   = (float *)nmf_alloc(max(m,n) * k * sizeof *N,   WRDLEN);
        D   = (float *)nmf_alloc(max(m,n) * k * sizeof *D,   WRDLEN);
        Aux = (float *)nmf_alloc(       k * k * sizeof *Aux, WRDLEN);
	
        if (N == NULL || D == NULL || Aux == NULL) { return -1; }

        for(i = 0; i < nIter; i++)
        {
           /* ************************ Phase 2 *************************** */
           /* N=A*H' */
           cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans,   m, k, n, 1.0, A, m, H, k, 0.0, N, m);

           switch (restr)
           {
              case 1:
              case 3:
                 srestr1norm_cpu(m*k, alphaW, N);
                 break;
              case 2:
              case 4:
                 srestr2norm_cpu(m*k, alphaW, W, N);
                 break;
           }

           /* Aux=H*H' */
           cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans,   k, k, n, 1.0, H, k, H, k, 0.0, Aux, k);

           /* D=W*Aux */
           cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, k, k, 1.0, W, m, Aux, k, 0.0, D, m);

           /* W=W(*)N(/)D */
           sdotdiv_cpu(m*k, N, D, W);
        }
        nmf_free(N);
        nmf_free(D);
        nmf_free(Aux);
        break;

     case UpdateH:
        N   = (float *)nmf_alloc(max(m,n) * k * sizeof *N,   WRDLEN);
        D   = (float *)nmf_alloc(max(m,n) * k * sizeof *D,   WRDLEN);
        Aux = (float *)nmf_alloc(       k * k * sizeof *Aux, WRDLEN);
	
        if (N == NULL || D == NULL || Aux == NULL) { return -1; }

        for(i = 0; i < nIter; i++)
        {
           /* ************************ Phase 1 *************************** */
           /* N=W'*A */	
           cblas_sgemm(CblasColMajor, CblasTrans,   CblasNoTrans, k, n, m, 1.0, W, m, A, m, 0.0, N, k);		
            
           switch (restr)
           {
              case 1:
              case 4:
                 srestr1norm_cpu(k*n, alphaH, N);
                 break;
              case 2:
              case 3:
                 srestr2norm_cpu(k*n, alphaH, H, N);
                 break;
           }

           /* Aux=W'*W */
           cblas_sgemm(CblasColMajor, CblasTrans,   CblasNoTrans, k, k, m, 1.0, W, m, W, m, 0.0, Aux, k);

           /* D=Aux*H */
           cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, k, n, k, 1.0, Aux, k, H, k, 0.0, D, k);

           /* H=H(*)N(/)D */
           sdotdiv_cpu(k*n, N, D, H);
        }
        nmf_free(N);
        nmf_free(D);
        nmf_free(Aux);
        break;

     default:
        return -1;
  } 
  return 0;
}


/**
 *  \fn    int dpbdivone_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const int uType, const int nIter)
 *  \brief dpbdivone_cpu performs NNMF using beta-divergence when beta=1, using double precision
 *
 *         The algorithm is<BR>
 *         &nbsp;&nbsp;repit nIter times<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;STEP 1<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;y(i)=sum(W(j,i) for all j in range) for all i in range<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L=W*H<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L=A(./)L<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B=W'*L<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B(i,j)=B(i,j) / y(i) for all B elements<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;H=H(.*)B<BR>
 *
 *         &nbsp;&nbsp;&nbsp;&nbsp;STEP 2<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;y(i)=sum(H(i,j) for all j in range) for all i in range<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L=W*H<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L=A(./)L <BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D=L*H'<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B(i,j)=B(i,j) / y(j) for all B elements<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;W=W(.*)D<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;end repit<BR>
 *         End algorithm<BR>
 *
 *         In real live B is a (k*n) matrix used in STEP 1, and D is a (m*k)
 *         matrix used in STEP 2. B and D are independent. For this reason only 1 matrix
 *         of size max(m,n)*k is declared/used
 *
 *  \param m:     (input) Number of rows of matrix A and matrix W
 *  \param n:     (input) Number of columns of matrix A and matrix H
 *  \param k:     (input) Number of columns of matrix W and rows of H
 *  \param A:     (input) Double precision input matrix of (m * n) number of elements stored using 1D column-major
 *  \param W:     (inout) Double precision input/output matrix of (m * k) number of elements stored using 1D column-major
 *  \param H:     (inout) Double precision input/output matrix of (k * n) number of elements stored using 1D column-major
 *  \param uType: (input) It can be UpdateAll (W and H are updated), UpdateW (only H is updated) or UpdateH (only W is updated)
 *  \param nIter: (input) Number of iterations
 *
 *  It returns 0 if all is OK.
*/
int dpbdivone_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const int uType, const int nIter)
{
   double *B=NULL, *L=NULL, *x=NULL, *y=NULL;
   int     i;

   switch (uType)
   {
     case UpdateAll:
        dbdivone_cpu(m, n, k, A, W, H, nIter);
        break;

     case UpdateW:
        B = (double *)nmf_alloc(m * k * sizeof *B, WRDLEN);
        L = (double *)nmf_alloc(m * n * sizeof *L, WRDLEN);
        x = (double *)nmf_alloc(    n * sizeof *x, WRDLEN);
        y = (double *)nmf_alloc(    k * sizeof *y, WRDLEN);
	
        if (B == NULL || L == NULL || x == NULL || y ==NULL) { return -1; }

        /* x[i]=1.0 for all i */
        dmemset_cpu(n, x, 1.0);

        for(i=0;i<nIter;i++)
        {
           /* ************************ Phase 2 *************************** */
           /* Calculate the sums of all H rows via dgemv(H, x) */
           cblas_dgemv(CblasColMajor, CblasNoTrans, k, n, 1.0, H, k, x, 1, 0.0, y, 1);

           /* L=W*H */
           cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);

           /* L=A(./)L*/
           ddiv_cpu(m*n, A, L, L);

           /* B=L*H' */
           cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans, m, k, n, 1.0, L, m, H, k, 0.0, B, m);

           /* B(i,j)=B(i,j)/y(j) for all B elements */
           /* W=W(.*)B */
           dupdate2W_cpu(m, k, y, B, W);
        }
        nmf_free(B);
        nmf_free(L);
        nmf_free(x);
        nmf_free(y);
        break;

     case UpdateH:
        B = (double *)nmf_alloc(n * k * sizeof *B, WRDLEN);
        L = (double *)nmf_alloc(m * n * sizeof *L, WRDLEN);
        x = (double *)nmf_alloc(    m * sizeof *x, WRDLEN);
        y = (double *)nmf_alloc(    k * sizeof *y, WRDLEN);
	
        if (B == NULL || L == NULL || x == NULL || y ==NULL) { return -1; }

        /* x[i]=1.0 for all i */
        dmemset_cpu(m, x, 1.0);

        for(i=0;i<nIter;i++)
        {
           /* ************************ Phase 1 *************************** */
           /* Calculate the sums of all W columns via dgemv(W, x) */
           cblas_dgemv(CblasColMajor, CblasTrans, m, k, 1.0, W, m, x, 1, 0.0, y, 1);

           /* L=W*H */
           cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);

           /* L=A(./)L*/
           ddiv_cpu(m*n, A, L, L);

           /* B=W'*L */
           cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, n, m, 1.0, W, m, L, m, 0.0, B, k);

           /* B(i,j)=B(i,j)/y(i) for all B elements */
           /* H=H(.*)B */
           dupdate2H_cpu(k, n, y, B, H);
        }
        nmf_free(B);
        nmf_free(L);
        nmf_free(x);
        nmf_free(y);
        break;

     default:
        return -1;
  } 
  return 0;
}


/**
 *  \fn    int spbdivone_cpu(const int m, const int n, const int k, const float *A, float *W, float *H, const int uType, const int nIter)
 *  \brief spbdivone_cpu performs NNMF using betadivergence when beta=1 using simple precision
 *  \param m:     (input) Number of rows of matrix A and matrix W
 *  \param n:     (input) Number of columns of matrix A and matrix H
 *  \param k:     (input) Number of columns of matrix W and rows of H
 *  \param A:     (input) Simple precision input matrix of (m * n) number of elements stored using 1D column-major
 *  \param W:     (inout) Simple precision input/output matrix of (m * k) number of elements stored using 1D column-major
 *  \param H:     (inout) Simple precision input/output matrix of (k * n) number of elements stored using 1D column-major
 *  \param uType: (input) It can be UpdateAll (W and H are updated), UpdateW (only H is updated) or UpdateH (only W is updated)
 *  \param nIter: (input) Number of iterations
 *  \return: 0 if all is OK, -1 otherwise  
*/
int spbdivone_cpu(const int m, const int n, const int k, const float *A, float *W, float *H, const int uType, const int nIter)
{
   float *B=NULL, *L=NULL, *x=NULL, *y=NULL;
   int    i;

   switch (uType)
   {
     case UpdateAll:
        sbdivone_cpu(m, n, k, A, W, H, nIter);
        break;

     case UpdateW:
        B = (float *)nmf_alloc(m * k * sizeof *B, WRDLEN);
        L = (float *)nmf_alloc(m * n * sizeof *L, WRDLEN);
        x = (float *)nmf_alloc(    n * sizeof *x, WRDLEN);
        y = (float *)nmf_alloc(    k * sizeof *y, WRDLEN);
	
        if (B == NULL || L == NULL || x == NULL || y ==NULL) { return -1; }

        /* x[i]=1.0 for all i */
        smemset_cpu(n, x, 1.0);

        for(i=0;i<nIter;i++)
        {
           /* ************************ Phase 2 *************************** */
           /* Calculate the sums of all H rows via sgemv(H, x) */
           cblas_sgemv(CblasColMajor, CblasNoTrans, k, n, 1.0, H, k, x, 1, 0.0, y, 1);

           /* L=W*H */
           cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);

           /* L=A(./)L*/
           sdiv_cpu(m*n, A, L, L);

           /* B=L*H' */
           cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans, m, k, n, 1.0, L, m, H, k, 0.0, B, m);

           /* B(i,j)=B(i,j)/y(j) for all B elements */
           /* W=W(.*)B */
           supdate2W_cpu(m, k, y, B, W);
        }
        nmf_free(B);
        nmf_free(L);
        nmf_free(x);
        nmf_free(y);
        break;

     case UpdateH:
        B = (float *)nmf_alloc(n * k * sizeof *B, WRDLEN);
        L = (float *)nmf_alloc(m * n * sizeof *L, WRDLEN);
        x = (float *)nmf_alloc(    m * sizeof *x, WRDLEN);
        y = (float *)nmf_alloc(    k * sizeof *y, WRDLEN);
	
        if (B == NULL || L == NULL || x == NULL || y ==NULL) { return -1; }

        /* x[i]=1.0 for all i */
        smemset_cpu(m, x, 1.0);

        for(i=0;i<nIter;i++)
        {
           /* ************************ Phase 1 *************************** */    
           /* Calculate the sums of all W columns via sgemv(W, x) */
           cblas_sgemv(CblasColMajor, CblasTrans, m, k, 1.0, W, m, x, 1, 0.0, y, 1);

           /* L=W*H */
           cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);

           /* L=A(./)L*/
           sdiv_cpu(m*n, A, L, L);

           /* B=W'*L */
           cblas_sgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, n, m, 1.0, W, m, L, m, 0.0, B, k);

           /* B(i,j)=B(i,j)/y(i) for all B elements */
           /* H=H(.*)B */
           supdate2H_cpu(k, n, y, B, H);
        }
        nmf_free(B);
        nmf_free(L);
        nmf_free(x);
        nmf_free(y);
        break;

     default:
        return -1;
  } 
  return 0;
}


/**TODO documentar
 * 
 */
int dpbdivoneRestrict_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const int uType, 
                          const int nIter, const double alphaW, const double alphaH, const unsigned short restr)
{
   double *B=NULL, *L=NULL, *x=NULL, *y=NULL;
   int     i;

   if ((alphaW == 0 && alphaH == 0) || restr == 0)
      return dpbdivone_cpu(m, n, k, A, W, H, UpdateAll, nIter);

   if(restr > 4) { return -2; }

   switch (uType)
   {
     case UpdateAll:
        dbdivoneRestrict_cpu(m, n, k, A, W, H, nIter, alphaW,  alphaH,  restr);
        break;

     case UpdateW:
        B = (double *)nmf_alloc(m * k * sizeof *B, WRDLEN);
        L = (double *)nmf_alloc(m * n * sizeof *L, WRDLEN);
        x = (double *)nmf_alloc(    n * sizeof *x, WRDLEN);
        y = (double *)nmf_alloc(    k * sizeof *y, WRDLEN);
	
        if (B == NULL || L == NULL || x == NULL || y ==NULL) { return -1; }

        /* x[i]=1.0 for all i */
        dmemset_cpu(n, x, 1.0);

        for(i=0;i<nIter;i++)
        {
           /* ************************ Phase 2 *************************** */
           /* Calculate the sums of all H rows via dgemv(H, x) */
           cblas_dgemv(CblasColMajor, CblasNoTrans, k, n, 1.0, H, k, x, 1, 0.0, y, 1);

           /* L=W*H */
           cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);

           /* L=A(./)L*/
           ddiv_cpu(m*n, A, L, L);

           /* B=L*H' */
           cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans, m, k, n, 1.0, L, m, H, k, 0.0, B, m);

           switch (restr)
           {
              case 1:
              case 3:
                 drestr1norm_cpu(m*k, alphaW, B);
                 break;
              case 2:
              case 4:
                 drestr2norm_cpu(m*k, alphaW, W, B);
                 break;
           }

           /* B(i,j)=B(i,j)/y(j) for all B elements */
           /* W=W(.*)B */
           dupdate2W_cpu(m, k, y, B, W);
        }
        nmf_free(B);
        nmf_free(L);
        nmf_free(x);
        nmf_free(y);
        break;

     case UpdateH:
        B = (double *)nmf_alloc(n * k * sizeof *B, WRDLEN);
        L = (double *)nmf_alloc(m * n * sizeof *L, WRDLEN);
        x = (double *)nmf_alloc(    m * sizeof *y, WRDLEN);
        y = (double *)nmf_alloc(    k * sizeof *x, WRDLEN);
	
        if (B == NULL || L == NULL || x == NULL || y ==NULL) { return -1; }

        /* x[i]=1.0 for all i */
        dmemset_cpu(m, x, 1.0);

        for(i=0;i<nIter;i++)
        {
           /* ************************ Phase 1 *************************** */
           /* Calculate the sums of all W columns via dgemv(W, x) */
           cblas_dgemv(CblasColMajor, CblasTrans, m, k, 1.0, W, m, x, 1, 0.0, y, 1);

           /* L=W*H */
           cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);

           /* L=A(./)L*/
           ddiv_cpu(m*n, A, L, L);

           /* B=W'*L */
           cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, n, m, 1.0, W, m, L, m, 0.0, B, k);

           switch (restr)
           {
              case 1:
              case 4:
                 drestr1norm_cpu(k*n, alphaH, B);
                 break;
              case 2:
              case 3:
                 drestr2norm_cpu(k*n, alphaH, H, B);
                 break;
           }

           /* B(i,j)=B(i,j)/y(i) for all B elements */
           /* H=H(.*)B */
           dupdate2H_cpu(k, n, y, B, H);
        }
        nmf_free(B);
        nmf_free(L);
        nmf_free(x);
        nmf_free(y);
        break;

     default:
        return -1;
  } 
  return 0;
}


/** TODO documentar 
 * 
 */
int spbdivoneRestrict_cpu(const int m, const int n, const int k, const float  *A, float  *W, float  *H, const int uType,
                          const int nIter, const float alphaW, const float alphaH, const unsigned short restr)
{
   float *B=NULL, *L=NULL, *x=NULL, *y=NULL;
   int    i;

   if ((alphaW == 0 && alphaH == 0) || restr == 0)
      return spbdivone_cpu(m, n, k, A, W, H, UpdateAll, nIter);

   if(restr > 4) { return -2; }

   switch (uType)
   {
     case UpdateAll:
        sbdivoneRestrict_cpu(m, n, k, A, W, H, nIter, alphaW,  alphaH,  restr);
        break;

     case UpdateW:
        /* Vectors "x" and "y" are used both in Phase 1 and Phase 2. With   */
        /* the strategy used for matrices B and D the sise of x is max(m,n) */
        B = (float *)nmf_alloc(max(m,n) * k * sizeof *B, WRDLEN);
        L = (float *)nmf_alloc(       m * n * sizeof *L, WRDLEN);
        x = (float *)nmf_alloc(    max(m,n) * sizeof *x, WRDLEN);
        y = (float *)nmf_alloc(           k * sizeof *y, WRDLEN);
	
        if (B == NULL || L == NULL || x == NULL || y ==NULL) { return -1; }

        /* x[i]=1.0 for all i */
        smemset_cpu(max(m,n), x, 1.0);

        for(i=0;i<nIter;i++)
        {
           /* ************************ Phase 2 *************************** */
           /* Calculate the sums of all H rows via dgemv(H, x) */
           cblas_sgemv(CblasColMajor, CblasNoTrans, k, n, 1.0, H, k, x, 1, 0.0, y, 1);

           /* L=W*H */
           cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);

           /* L=A(./)L*/
           sdiv_cpu(m*n, A, L, L);

           /* B=L*H' */
           cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans, m, k, n, 1.0, L, m, H, k, 0.0, B, m);

           switch (restr)
           {
              case 1:
              case 3:
                 srestr1norm_cpu(m*k, alphaW, B);
                 break;
              case 2:
              case 4:
                 srestr2norm_cpu(m*k, alphaW, W, B);
                 break;
           }

           /* B(i,j)=B(i,j)/y(j) for all B elements */
           /* W=W(.*)B */
           supdate2W_cpu(m, k, y, B, W);
        }
        nmf_free(B);
        nmf_free(L);
        nmf_free(x);
        nmf_free(y);
        break;

     case UpdateH:
        /* Vectors "x" and "y" are used both in Phase 1 and Phase 2. With   */
        /* the strategy used for matrices B and D the sise of x is max(m,n) */
        B = (float *)nmf_alloc(max(m,n) * k * sizeof *B, WRDLEN);
        L = (float *)nmf_alloc(       m * n * sizeof *L, WRDLEN);
        x = (float *)nmf_alloc(    max(m,n) * sizeof *y, WRDLEN);
        y = (float *)nmf_alloc(           k * sizeof *x, WRDLEN);
	
        if (B == NULL || L == NULL || x == NULL || y ==NULL) { return -1; }

        /* x[i]=1.0 for all i */
        smemset_cpu(max(m,n), x, 1.0);

        for(i=0;i<nIter;i++)
        {
           /* ************************ Phase 1 *************************** */
           /* Calculate the sums of all W columns via dgemv(W, x) */
           cblas_sgemv(CblasColMajor, CblasTrans, m, k, 1.0, W, m, x, 1, 0.0, y, 1);

           /* L=W*H */
           cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, L, m);

           /* L=A(./)L*/
           sdiv_cpu(m*n, A, L, L);

           /* B=W'*L */
           cblas_sgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, n, m, 1.0, W, m, L, m, 0.0, B, k);

           switch (restr)
           {
              case 1:
              case 4:
                 srestr1norm_cpu(k*n, alphaH, B);
                 break;
              case 2:
              case 3:
                 srestr2norm_cpu(k*n, alphaH, H, B);
                 break;
           }

           /* B(i,j)=B(i,j)/y(i) for all B elements */
           /* H=H(.*)B */
           supdate2H_cpu(k, n, y, B, H);
        }
        nmf_free(B);
        nmf_free(L);
        nmf_free(x);
        nmf_free(y);
        break;

     default:
        return -1;
  } 
  return 0;
}


/**
 *  \fn    int dpbdiv_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const double beta, const int uType, const int nIter)
 *  \brief dpbdiv_cpu is a wrapper that calls the adequate function to performs NNMF using betadivergence using double precision with CPU
 *  \param m:     (input) Number of rows of matrix A and matrix W
 *  \param n:     (input) Number of columns of matrix A and matrix H
 *  \param k:     (input) Number of columns of matrix W and rows of H
 *  \param A:     (input) Double precision input matrix of (m * n) number of elements stored using 1D column-major
 *  \param W:     (inout) Double precision input/output matrix of (m * k) number of elements stored using 1D column-major
 *  \param H:     (inout) Double precision input/output matrix of (k * n) number of elements stored using 1D column-major
 *  \param beta:  (input) Double precision value. The parameter beta of betadivergence method
 *  \param uType: (input) It can be UpdateAll (W and H are updated), UpdateW (only H is updated) or UpdateH (only W is updated)
 *  \param nIter: (input) Number of iterations
 *  \return: 0 if all is OK, -1 otherwise  
*/
int dpbdiv_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const double beta, const int uType, const int nIter)
{
   if ((beta < 0.0) || (nIter <= 0)) { return -1; }

   if(beta>=2.0 && beta<=2.0)
     return dpmlsa_cpu(m, n, k, A, W, H, uType, nIter);
   else
   {
     if(beta>=1.0 && beta<=1.0)
       return dpbdivone_cpu(m, n, k, A, W, H, uType, nIter);
     else
       return dpbdivg_cpu(m, n, k, A, W, H, beta, uType, nIter);
   }
}


/**
 *  \fn    int spbdiv_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const float beta, const int uType, const int nIter)
 *  \brief spbdiv_cpu is a wrapper that calls the adequate function to performs NNMF using betadivergence using simple precision with CPU
 *  \param m:     (input) Number of rows of matrix A and matrix W
 *  \param n:     (input) Number of columns of matrix A and matrix H
 *  \param k:     (input) Number of columns of matrix W and rows of H
 *  \param A:     (input) Simple precision input matrix of (m * n) number of elements stored using 1D column-major
 *  \param W:     (inout) Simple precision input/output matrix of (m * k) number of elements stored using 1D column-major
 *  \param H:     (inout) Simple precision input/output matrix of (k * n) number of elements stored using 1D column-major
 *  \param beta:  (input) Simple precision value. The parameter beta of betadivergence method
 *  \param uType: (input) It can be UpdateAll (W and H are updated), UpdateW (only H is updated) or UpdateH (only W is updated)
 *  \param nIter: (input) Number of iterations
*/
int spbdiv_cpu(const int m, const int n, const int k, const float *A, float *W, float *H, const float beta, const int uType, const int nIter)
{
   if ((beta < 0.0) || (nIter <= 0)) { return -1; }

   if(beta>=2.0 && beta<=2.0)
     return spmlsa_cpu(m, n, k, A, W, H, uType, nIter);
   else
   {
     if(beta>=1.0 && beta<=1.0)
       return spbdivone_cpu(m, n, k, A, W, H, uType, nIter);
     else
       return spbdivg_cpu(m, n, k, A, W, H, beta, uType, nIter);
   }
}


/** TODO documentar
 * 
 */
int dpbdivRestrict_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const double beta, const int uType, 
                       const int nIter, const double alphaW, const double alphaH, const unsigned short restr)
{
   if ((beta < 0.0) || (nIter <= 0)) { return -1; }

   if(beta>=2.0 && beta<=2.0)
     return dpmlsaRestrict_cpu(m, n, k, A, W, H, uType, nIter, alphaW, alphaH, restr);
   else
   {
     if(beta>=1.0 && beta<=1.0)
       return dpbdivoneRestrict_cpu(m, n, k, A, W, H, uType, nIter, alphaW, alphaH, restr);
     else
       return dpbdivgRestrict_cpu(m, n, k, A, W, H, beta, uType, nIter, alphaW, alphaH, restr);
   }
}


/** TODO documentar
 * 
 */
int spbdivRestrict_cpu(const int m, const int n, const int k, const float *A, float *W, float *H, const float beta, const int uType, 
                       const int nIter, const float alphaW, const float alphaH, const unsigned short restr)
{
   if ((beta < 0.0) || (nIter <= 0)) { return -1; }

   if(beta>=2.0 && beta<=2.0)
     return spmlsaRestrict_cpu(m, n, k, A, W, H, uType, nIter, alphaW, alphaH, restr);
   else
   {
     if(beta>=1.0 && beta<=1.0)
       return spbdivoneRestrict_cpu(m, n, k, A, W, H, uType, nIter, alphaW, alphaH, restr);
     else
       return spbdivgRestrict_cpu(m, n, k, A, W, H, beta, uType, nIter, alphaW, alphaH, restr);
   }
}
