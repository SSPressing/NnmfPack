/**
 *  \file    gcd_cpu.c
 *  \brief   File with functions to calcule NNMF using the fast GCD algorithm for CPUs
 *  \author  P. San juan
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \date    08/05/18
*/

#include "gcd_cpu.h"

/** TODO documentar
 * 
 */
int dGCD_cpu(const int m, const int n, const int k, const double* A, double *W, double *H, const int nIter, const double epCD)
{
   double *Gw=NULL, *Sw=NULL, *Dw=NULL, *Pww=NULL, *Phh=NULL, *Pvh=NULL, *Pwv=NULL;
   double *Gh=NULL, *Dh=NULL, *Sh=NULL, *mm=NULL,  pinit, sa;
   int    *q, i, j, cont, iter;
    
   Pww = (double *)nmf_alloc(k * k    * sizeof *Pww, WRDLEN);
   Phh = (double *)nmf_alloc(k * k    * sizeof *Phh, WRDLEN);
   Pvh = (double *)nmf_alloc(m * k    * sizeof *Pvh, WRDLEN);
   Pwv = (double *)nmf_alloc(k * n    * sizeof *Pwv, WRDLEN);
   Gw  = (double *)nmf_alloc(m * k    * sizeof *Gw,  WRDLEN);
   Sw  = (double *)nmf_alloc(m * k    * sizeof *Sw,  WRDLEN);
   Dw  = (double *)nmf_alloc(m * k    * sizeof *Dw,  WRDLEN);
   Gh  = (double *)nmf_alloc(k * n    * sizeof *Gh,  WRDLEN);
   Sh  = (double *)nmf_alloc(k * n    * sizeof *Sh,  WRDLEN);
   Dh  = (double *)nmf_alloc(k * n    * sizeof *Dh,  WRDLEN);
   q   = (int *)   nmf_alloc(max(m,n) * sizeof *q,   WRDLEN);
   mm  = (double *)nmf_alloc(max(m,n) * sizeof *mm,  WRDLEN);

   if (Pww==NULL || Phh==NULL || Pvh==NULL || Pwv==NULL || Gw==NULL || Sw==NULL || Dw==NULL) { return -1; }
   if (Gh ==NULL || Sh ==NULL || Dh ==NULL || mm ==NULL || q ==NULL) { return -1; }
    
   //normalization to unit l2-norm length
   dnrm2Cols_cpu(m, k, W);

   for(cont=0; cont<nIter; cont++)
   {
      //Phh=H*H';
      //Pvh=V*H'; 
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans, k, k, n, 1.0, H, k, H, k, 0.0, Phh, k);
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans, m, k, n, 1.0, A, m, H, k, 0.0, Pvh, m);

      //ACTUALIZACION DE W    
      //Calculo de Gw, Sw y Dw iniciales
      //Gw=W*Phh-Pvh; //TODO posible optimizacion aqui
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, k, k, 1.0, W, m, Phh, k, 0.0, Gw, m);
      cblas_daxpy(m*k, -1, Pvh, 1, Gw, 1);

      for(i=0;i<m;i++)
      {
         dupdaSwDw(i, k, W, Gw, Phh, m, Sw, Dw);
			
         //[mm(i),q(i)]=max(Dw(i,:)); //TODO revisar si Dw puede contener negativos y el idx
         q[i] =cblas_idamax(k, &Dw[i], m);
         mm[i] = Dw[i + q[i] * m];
      }

      //pinit=max(mm);
      pinit = mm[cblas_idamax(m, mm, 1)];

      //Barrido de la matriz Dw por filas
      for(i=0; i<m; i++)
      {
         iter = 0;

         // Actualizacion del elemento con mayor decrecimiento
         while((Dw[i + q[i] * m]>epCD*pinit) && (iter < k*k))
         {
            //sa=Sw(i,q(i));
            sa = Sw[i + q[i]*m];

            //Actualizacion de W 7.3
            //W(i,q(i))=W(i,q(i))+sa;  
            W[i + q[i] *m] += sa;

            //Actualizacion de Gw al cambiar W 7.4
            //Gw(i,:)=Gw(i,:)+sa*Phh(q(i),:);
            cblas_daxpy(k, sa, &Phh[q[i]], k, &Gw[i], m);
            
            //Actualizacion de Sw y Dw al cambiar W 7.5 7.6		
            dupdaSwDw(i,k, W, Gw, Phh, m, Sw, Dw);
			   
            //[mm(i),q(i)]=max(Dw(i,:)); //TODO revisar si Dw puede contener negativos y el idx
            q[i] =cblas_idamax(k, &Dw[i], m);
         }
      }
       
      //ACTUALIZACION DE H
      //Pww=W'*W;
      //Pwv=W'*V;
      cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, k, m, 1.0, W, m, W, m, 0.0, Pww, k);
      cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, n, m, 1.0, W, m, A, m, 0.0, Pwv, k);

      //Calculo de Gh, Sh y Dh iniciales
      //Gh=Pww*H-Pwv;
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, k, n, k, 1.0, Pww, k, H, k, 0.0, Gh, k);
      cblas_daxpy(k*n, -1, Pwv, 1, Gh, 1);
        
      for(j=0; j<n; j++)
      {
         dupdaShDh(j,k, H, Gh, Pww, Sh, Dh);
         //[mm(j),q(j)]=max(Dh(:,j));
         q[j] =cblas_idamax(k, &Dh[j*k], 1);
         mm[j] = Dh[q[j] + j * k];
      }
      //pinit=max(mm);
      pinit = mm[cblas_idamax(n, mm, 1)];

      //Barrido de la matriz Dh por columnas
      for(j=0; j<n; j++)
      {
         iter = 0;
         //Actualizacion del elemento con mayor decrecimiento
         //while Dh(q(j),j)>EpCD*pinit
         while( (Dh[q[j] + j*k]>epCD*pinit) && (iter < k*k))
         {
            //sa=Sh(q(j),j);
            sa = Sh[q[j] + j*k];     

            //Actualizacion de H
            //H(q(j),j)=H(q(j),j)+sa;              
            H[q[j] + j * k] += sa; 

            //Actualizacion de Gh al cambiar H
            //Gh(:,j)=Gh(:,j)+sa*Pww(:,q(j));
            cblas_daxpy(k, sa, &Pww[q[j]*k], 1, &Gh[j*k], 1);

            //Actualizacion de Sh y Dh al cambiar H 
            dupdaShDh(j,k, H, Gh, Pww, Sh, Dh);

            //[mm(j),q(j)]=max(Dh(:,j));  
            q[j] =cblas_idamax(k, &Dh[j*k], 1);
                
            iter++;
         }
      }
   }

   nmf_free(Pww); nmf_free(Phh); nmf_free(Pvh); nmf_free(Pwv);
   nmf_free(Gw);  nmf_free(Sw);  nmf_free(Dw);  nmf_free(q);
   nmf_free(mm);  nmf_free(Gh);  nmf_free(Sh);  nmf_free(Dh);

   return 0;
}


/** TODO documentar
 * 
 */
int sGCD_cpu(const int m, const int n, const int k, const float* A, float *W, float *H, const int nIter, const float epCD)
{
   float *Gw=NULL, *Sw=NULL, *Dw=NULL, *Pww=NULL, *Phh=NULL, *Pvh=NULL, *Pwv=NULL;
   float *Gh=NULL, *Dh=NULL, *Sh=NULL, *mm=NULL,  pinit, sa;
   int   *q, i, j, cont, iter;
    
   Pww = (float *)nmf_alloc(k * k    * sizeof *Pww, WRDLEN);
   Phh = (float *)nmf_alloc(k * k    * sizeof *Phh, WRDLEN);
   Pvh = (float *)nmf_alloc(m * k    * sizeof *Pvh, WRDLEN);
   Pwv = (float *)nmf_alloc(k * n    * sizeof *Pwv, WRDLEN);
   Gw  = (float *)nmf_alloc(m * k    * sizeof *Gw,  WRDLEN);
   Sw  = (float *)nmf_alloc(m * k    * sizeof *Sw,  WRDLEN);
   Dw  = (float *)nmf_alloc(m * k    * sizeof *Dw,  WRDLEN);
   Gh  = (float *)nmf_alloc(k * n    * sizeof *Gh,  WRDLEN);
   Sh  = (float *)nmf_alloc(k * n    * sizeof *Sh,  WRDLEN);
   Dh  = (float *)nmf_alloc(k * n    * sizeof *Dh,  WRDLEN);
   q   = (int *)  nmf_alloc(max(m,n) * sizeof *q,   WRDLEN);
   mm  = (float *)nmf_alloc(max(m,n) * sizeof *mm,  WRDLEN);

   if (Pww==NULL || Phh==NULL || Pvh==NULL || Pwv==NULL || Gw==NULL || Sw==NULL || Dw==NULL) { return -1; }
   if (Gh ==NULL || Sh ==NULL || Dh ==NULL || mm ==NULL || q ==NULL) { return -1; }
    
   //normalization to unit l2-norm length
   snrm2Cols_cpu(m, k, W);

   for(cont=0; cont<nIter; cont++)
   {
      //Phh=H*H';
      //Pvh=V*H'; 
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans, k, k, n, 1.0, H, k, H, k, 0.0, Phh, k);
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans, m, k, n, 1.0, A, m, H, k, 0.0, Pvh, m);

      //ACTUALIZACION DE W    
      //Calculo de Gw, Sw y Dw iniciales
      //Gw=W*Phh-Pvh; //TODO posible optimizacion aqui
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, k, k, 1.0, W, m, Phh, k, 0.0, Gw, m);
      cblas_saxpy(m*k, -1, Pvh, 1, Gw, 1);

      for(i=0;i<m;i++)
      {
         supdaSwDw(i, k, W, Gw, Phh, m, Sw, Dw);
			
         //[mm(i),q(i)]=max(Dw(i,:)); //TODO revisar si Dw puede contener negativos y el idx
         q[i] =cblas_isamax(k, &Dw[i], m);
         mm[i] = Dw[i + q[i] * m];
      }

      //pinit=max(mm);
      pinit = mm[cblas_isamax(m, mm, 1)];

      //Barrido de la matriz Dw por filas
      for(i=0; i<m; i++)
      {
         iter = 0;

         // Actualizacion del elemento con mayor decrecimiento
         while((Dw[i + q[i] * m]>epCD*pinit) && (iter < k*k))
         {
            //sa=Sw(i,q(i));
            sa = Sw[i + q[i]*m];

            //Actualizacion de W 7.3
            //W(i,q(i))=W(i,q(i))+sa;  
            W[i + q[i] *m] += sa;

            //Actualizacion de Gw al cambiar W 7.4
            //Gw(i,:)=Gw(i,:)+sa*Phh(q(i),:);
            cblas_saxpy(k, sa, &Phh[q[i]], k, &Gw[i], m);
            
            //Actualizacion de Sw y Dw al cambiar W 7.5 7.6		
            supdaSwDw(i,k, W, Gw, Phh, m, Sw, Dw);
			   
            //[mm(i),q(i)]=max(Dw(i,:)); //TODO revisar si Dw puede contener negativos y el idx
            q[i] =cblas_isamax(k, &Dw[i], m);
         }
      }
       
      //ACTUALIZACION DE H
      //Pww=W'*W;
      //Pwv=W'*V;
      cblas_sgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, k, m, 1.0, W, m, W, m, 0.0, Pww, k);
      cblas_sgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, n, m, 1.0, W, m, A, m, 0.0, Pwv, k);

      //Calculo de Gh, Sh y Dh iniciales
      //Gh=Pww*H-Pwv;
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, k, n, k, 1.0, Pww, k, H, k, 0.0, Gh, k);
      cblas_saxpy(k*n, -1, Pwv, 1, Gh, 1);
        
      for(j=0; j<n; j++)
      {
         supdaShDh(j,k, H, Gh, Pww, Sh, Dh);
         //[mm(j),q(j)]=max(Dh(:,j));
         q[j] =cblas_isamax(k, &Dh[j*k], 1);
         mm[j] = Dh[q[j] + j * k];
      }
      //pinit=max(mm);
      pinit = mm[cblas_isamax(n, mm, 1)];

      //Barrido de la matriz Dh por columnas
      for(j=0; j<n; j++)
      {
         iter = 0;
         //Actualizacion del elemento con mayor decrecimiento
         //while Dh(q(j),j)>EpCD*pinit
         while( (Dh[q[j] + j*k]>epCD*pinit) && (iter < k*k))
         {
            //sa=Sh(q(j),j);
            sa = Sh[q[j] + j*k];     

            //Actualizacion de H
            //H(q(j),j)=H(q(j),j)+sa;              
            H[q[j] + j * k] += sa; 

            //Actualizacion de Gh al cambiar H
            //Gh(:,j)=Gh(:,j)+sa*Pww(:,q(j));
            cblas_saxpy(k, sa, &Pww[q[j]*k], 1, &Gh[j*k], 1);

            //Actualizacion de Sh y Dh al cambiar H 
            supdaShDh(j,k, H, Gh, Pww, Sh, Dh);

            //[mm(j),q(j)]=max(Dh(:,j));  
            q[j] =cblas_isamax(k, &Dh[j*k], 1);
                
            iter++;
         }
      }
   }

   nmf_free(Pww); nmf_free(Phh); nmf_free(Pvh); nmf_free(Pwv);
   nmf_free(Gw);  nmf_free(Sw);  nmf_free(Dw);  nmf_free(q);
   nmf_free(mm);  nmf_free(Gh);  nmf_free(Sh);  nmf_free(Dh);

   return 0;
}


//TODO documentar
void dupdaSwDw(const int i, const int k, const double *W, const double *Gw, const double *Phh, const int m, double *__restrict__ Sw, double *__restrict__ Dw)
{
   int r;

   #ifdef With_ICC
     #pragma loop_count min=32
     #pragma omp simd
   #else
     #pragma omp parallel for
   #endif
   for(r=0; r<k; r++)
   {
      //Sw(i,r)=max(W(i,r)-Gw(i,r)/Phh(r,r),0)-W(i,r);
      Sw[i + r*m] = max(W[i + r*m] - Gw[i + r*m]/Phh[r + r*k],0) - W[i + r*m];

      //Dw(i,r)=-Gw(i,r)*Sw(i,r)-0.5*Phh(r,r)*(Sw(i,r)^2);
      Dw[i + r*m] = -Gw[i + r*m] * Sw[i + r*m] - 0.5*Phh[r + r*k] * Sw[i + r*m] * Sw[i + r*m];
   }  
}			   


//TODO documentar
void dupdaShDh(const int j, const int k, const double *H, const double *Gh, const double *Pww, double *__restrict__ Sh, double *__restrict__ Dh)
{
  int r;

   #ifdef With_ICC
     #pragma loop_count min=32
     #pragma omp simd
   #else
     #pragma omp parallel for
   #endif
   for(r=0; r<k; r++)
   {
      //Sh(r,j)=max(H(r,j)-Gh(r,j)/Pww(r,r),0)-H(r,j);
      Sh[r + j*k] = max(H[r + j*k] - Gh[r + j*k]/Pww[r + r*k],0) - H[r + j*k];

      //Dh(r,j)=-Gh(r,j)*Sh(r,j)-0.5*Pww(r,r)*(Sh(r,j)^2);
      Dh[r + j*k] = -Gh[r + j*k] * Sh[r + j*k] - 0.5*Pww[r + r*k] * Sh[r + j*k] * Sh[r + j*k];
   }
}		


//TODO documentar
void supdaSwDw(const int i, const int k, const float *W, const float *Gw, const float *Phh, const int m, float *__restrict__ Sw, float *__restrict__ Dw)
{
   int r;

   #ifdef With_ICC
     #pragma loop_count min=32
     #pragma omp simd
   #else
     #pragma omp parallel for
   #endif
   for(r=0; r<k; r++)
   {
      //Sw(i,r)=max(W(i,r)-Gw(i,r)/Phh(r,r),0)-W(i,r);
      Sw[i + r*m] = max(W[i + r*m] - Gw[i + r*m]/Phh[r + r*k],0) - W[i + r*m];

      //Dw(i,r)=-Gw(i,r)*Sw(i,r)-0.5*Phh(r,r)*(Sw(i,r)^2);
      Dw[i + r*m] = -Gw[i + r*m] * Sw[i + r*m] - 0.5*Phh[r + r*k] * Sw[i + r*m] * Sw[i + r*m];
   }  
}			   


//TODO documentar
void supdaShDh(const int j, const int k, const float *H, const float *Gh, const float *Pww, float *__restrict__ Sh, float *__restrict__ Dh)
{
  int r;

   #ifdef With_ICC
     #pragma loop_count min=32
     #pragma omp simd
   #else
     #pragma omp parallel for
   #endif
   for(r=0; r<k; r++)
   {
      //Sh(r,j)=max(H(r,j)-Gh(r,j)/Pww(r,r),0)-H(r,j);
      Sh[r + j*k] = max(H[r + j*k] - Gh[r + j*k]/Pww[r + r*k],0) - H[r + j*k];

      //Dh(r,j)=-Gh(r,j)*Sh(r,j)-0.5*Pww(r,r)*(Sh(r,j)^2);
      Dh[r + j*k] = -Gh[r + j*k] * Sh[r + j*k] - 0.5*Pww[r + r*k] * Sh[r + j*k] * Sh[r + j*k];
   }
}		

