/**
 *  \file    fhals_cpu.c
 *  \brief   File with functions to calcule NNMF using the fast HALS algorithm for CPUs
 *  \author  P. San juan
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \date    08/05/18
*/

#include "fhals_cpu.h"

/** TODO documentar
 * 
 */
int dfhals_cpu(const int m, const int n, const int k, const double* A, double *W, double *H, const int nIter)
{
   double *B=NULL, *Mat1=NULL, *Mat2=NULL, norm;
   int     i, j;
    
   B    = (double *)nmf_alloc(n *        k * sizeof *B,    WRDLEN);
   Mat1 = (double *)nmf_alloc(max(m,n) * k * sizeof *Mat1, WRDLEN);
   Mat2 = (double *)nmf_alloc(k *        k * sizeof *Mat2, WRDLEN);

   if (B == NULL || Mat1 == NULL || Mat2 == NULL) { return -1; }

   dtrans_cpu(k, n, H, B);

   //normalization to unit l2-norm length
   dnrm2Cols_cpu(m, k, W);
      
   for(i=0; i<nIter; i++)
   {
      //Mat1 = A'* W;
      cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, n, k, m, 1.0, A, m, W, m, 0.0, Mat1, n);

      //MAt2 = W' * W;
      cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, k, m, 1.0, W, m, W, m, 0.0, Mat2, k);
        
      for(j = 0; j < k; j++)
      {
         //B(:,j) = max( B(:,j) + Mat1(:,j) - B * Mat2(:,j), dEPS)
         //B(:,j) = max( B(:,j) + - B * Mat2(:,j) + Mat1(:,j), dEPS)
         cblas_dgemv(CblasColMajor, CblasNoTrans, n, k, -1.0, B, n, &Mat2[j*k], 1, 1.0, &Mat1[j*n],1);
         cblas_daxpy(n, 1.0, &Mat1[j*n], 1, &B[j*n], 1);
         dhalfwave_cpu(n, &B[j*n], 1);
      }
        
      //Mat3 = A * B
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, k, n, 1.0, A, m, B, n, 0.0, Mat1, m);
        
      //Mat2 = B' * B
      cblas_dgemm(CblasColMajor, CblasTrans,   CblasNoTrans, k, k, n, 1.0, B, n, B, n, 0.0, Mat2, k);
        
      for(j = 0; j < k; j++)
      {
         //W(:,j) = max( W(:,j) * Mat2(j,j) + Mat3(:,j) - W * Mat2(:,j) , 0)
         cblas_dgemv(CblasColMajor , CblasNoTrans, m, k, -1.0, W, m, &Mat2[j*k], 1, 1.0, &Mat1[j*m],1);
         cblas_daxpy(m, Mat2[j+j*k], &W[j*m], 1, &Mat1[j*m], 1);
         cblas_dcopy(m, &Mat1[j*m], 1, &W[j*m], 1);
         dhalfwave_cpu(m, &W[j*m], 1);
            
         //W(:,j) = W(:,j) / norm(W(:,j));
         norm = cblas_dnrm2(m, &W[j*m], 1);
         cblas_dscal(m, 1.0/norm, &W[j*m], 1);
      }
   }
   dtrans_cpu(n, k, B, H);
    
   nmf_free(B);
   nmf_free(Mat1);
   nmf_free(Mat2);

   return 0;
}


/** TODO documentar
 * 
 */
int sfhals_cpu(const int m, const int n, const int k, const float* A, float *W, float *H, const int nIter)
{
   float *B=NULL, *Mat1=NULL, *Mat2=NULL, norm;
   int    i, j;
    
   B    = (float *)nmf_alloc(n *        k * sizeof *B,    WRDLEN);
   Mat1 = (float *)nmf_alloc(max(m,n) * k * sizeof *Mat1, WRDLEN);
   Mat2 = (float *)nmf_alloc(k *        k * sizeof *Mat2, WRDLEN);

   if (B == NULL || Mat1 == NULL || Mat2 == NULL) { return -1; }

   strans_cpu(k, n, H, B);

   //normalization to unit l2-norm length
   snrm2Cols_cpu(m, k, W);
      
   for(i=0; i<nIter; i++)
   {
      //Mat1 = A'* W;
      cblas_sgemm(CblasColMajor, CblasTrans, CblasNoTrans, n, k, m, 1.0, A, m, W, m, 0.0, Mat1, n);

      //MAt2 = W' * W;
      cblas_sgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, k, m, 1.0, W, m, W, m, 0.0, Mat2, k);
        
      for(j = 0; j < k; j++)
      {
         //B(:,j) = max( B(:,j) + Mat1(:,j) - B * Mat2(:,j), dEPS)
         //B(:,j) = max( B(:,j) + - B * Mat2(:,j) + Mat1(:,j), dEPS)
         cblas_sgemv(CblasColMajor, CblasNoTrans, n, k, -1.0, B, n, &Mat2[j*k], 1, 1.0, &Mat1[j*n],1);
         cblas_saxpy(n, 1.0, &Mat1[j*n], 1, &B[j*n], 1);
         shalfwave_cpu(n, &B[j*n], 1);
      }
        
      //Mat3 = A * B
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, k, n, 1.0, A, m, B, n, 0.0, Mat1, m);
        
      //Mat2 = B' * B
      cblas_sgemm(CblasColMajor, CblasTrans,   CblasNoTrans, k, k, n, 1.0, B, n, B, n, 0.0, Mat2, k);
        
      for(j = 0; j < k; j++)
      {
         //W(:,j) = max( W(:,j) * Mat2(j,j) + Mat3(:,j) - W * Mat2(:,j) , 0)
         cblas_sgemv(CblasColMajor , CblasNoTrans, m, k, -1.0, W, m, &Mat2[j*k], 1, 1.0, &Mat1[j*m],1);
         cblas_saxpy(m, Mat2[j+j*k], &W[j*m], 1, &Mat1[j*m], 1);
         cblas_scopy(m, &Mat1[j*m], 1, &W[j*m], 1);
         shalfwave_cpu(m, &W[j*m], 1);
            
         //W(:,j) = W(:,j) / norm(W(:,j));
         norm = cblas_snrm2(m, &W[j*m], 1);
         cblas_sscal(m, 1.0/norm, &W[j*m], 1);
      }
   }
   strans_cpu(n, k, B, H);
    
   nmf_free(B);
   nmf_free(Mat1);
   nmf_free(Mat2);

   return 0;
}


