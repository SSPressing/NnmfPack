/***************************************************************************
 *   Copyright (C) 2014 by PIR (University of Oviedo) and                  *
 *   INCO2 (Polytechnic University of Valencia) groups.                    *
 *    nmfpack@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************
*/
/**
 *  \file    mlsa_cpu.h
 *  \brief   File with functions to calcule NNMF using the mlsa algorithm for CPUs
 *  \author  Information Retrieval and Parallel Computing Group (IRPCG)
 *  \author  University of Oviedo, Spain
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \author  Contact: nmfpack@gmail.com
 *  \date    12/03/18
*/

#include "mlsa_cpu.h"

/**
 *  \fn    int dmlsa_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const int uType, const int nIter)
 *  \brief dmlsa_cpu performs NNMF using betadivergence when beta=2 using double precision
 *
 *         The algorithm is<BR>
 *         &nbsp;&nbsp;repit nIter times<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;STEP 1<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L=W*H<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B=W'*A<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C=W'*L<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;H=H(.*)B(./)C<BR>
 *
 *         &nbsp;&nbsp;&nbsp;&nbsp;STEP 2<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L=W*H<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D=A*H'<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E=L*H'<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;W=W(.*)D(./)E<BR>
 *         &nbsp;&nbsp;&nbsp;&nbsp;end repit<BR>
 *         End algorithm<BR>
 *
 *         To save some FLOPs and RAM a modified Lee and Seung version is used, so we have Step 1: L=W'*W,
 *         C=L*H and then B=W'*A; Step 2: L=H*H', E=W*L and D=A*H'. W'*W (H*H') and L*H (W*L) can do in
 *         parallel with W'*A (A*H'). Because dgemm works fine we don't use threads to do in parallel.
 *
 *         In real live B and C are (k*n) matrices used in STEP 1, and D and E are (m*k)
 *         matrices used in STEP 2. B/C and D/E are independent. For this reason only 2 matrices are declared
 *         to save space. They are matrices B and C with size max(m,n)*k
 *
 *  \param m:     (input) Number of rows of matrix A and matrix W
 *  \param n:     (input) Number of columns of matrix A and matrix H
 *  \param k:     (input) Number of columns of matrix W and rows of H
 *  \param A:     (input) Double precision input matrix of (m * n) number of elements stored using 1D column-major
 *  \param W:     (inout) Double precision input/output matrix of (m * k) number of elements stored using 1D column-major
 *  \param H:     (inout) Double precision input/output matrix of (k * n) number of elements stored using 1D column-major
 *  \param nIter: (input) Number of iterations
 *  \return: 0 if all is OK, -1 otherwise 
*/
int dmlsa_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, const int nIter)
{
   double *B=NULL, *C=NULL, *L=NULL;
   int     i;

   B = (double *)nmf_alloc(max(m,n) * k * sizeof *B, WRDLEN);
   C = (double *)nmf_alloc(max(m,n) * k * sizeof *C, WRDLEN);
   L = (double *)nmf_alloc(       k * k * sizeof *L, WRDLEN);

   if (B == NULL || C == NULL || L == NULL) { return -1; }

   for(i=0;i<nIter;i++)
   {
      /* ************************ Phase 1 *************************** */
      /* B=W'*A */	
      cblas_dgemm(CblasColMajor, CblasTrans,   CblasNoTrans, k, n, m, 1.0, W, m, A, m, 0.0, B, k);		

      /* L=W'*W */
      cblas_dgemm(CblasColMajor, CblasTrans,   CblasNoTrans, k, k, m, 1.0, W, m, W, m, 0.0, L, k);

      /* C=L*H */
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, k, n, k, 1.0, L, k, H, k, 0.0, C, k);

      /* H=H(*)B(/)C */
      ddotdiv_cpu(k*n, B, C, H);

      /* ************************ Phase 2 *************************** */
      /* B=A*H' */
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans,   m, k, n, 1.0, A, m, H, k, 0.0, B, m);

      /* L=H*H' */
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans,   k, k, n, 1.0, H, k, H, k, 0.0, L, k);

      /* C=W*L */
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, k, k, 1.0, W, m, L, k, 0.0, C, m);

      /* W=W(*)B(/)C */
      ddotdiv_cpu(m*k, B, C, W);
   }

   nmf_free(B);
   nmf_free(C);
   nmf_free(L);

  return 0;
}


/**
 *  \fn    int smlsa_cpu(const int m, const int n, const int k, const float *A, float *W, float *H, const int uType, const int nIter)
 *  \brief smlsa_cpu performs NNMF using betadivergence when beta=2 using simple precision
 *  \param m:     (input) Number of rows of matrix A and matrix W
 *  \param n:     (input) Number of columns of matrix A and matrix H
 *  \param k:     (input) Number of columns of matrix W and rows of H
 *  \param A:     (input) Simple precision input matrix of (m * n) number of elements stored using 1D column-major
 *  \param W:     (inout) Simple precision input/output matrix of (m * k) number of elements stored using 1D column-major
 *  \param H:     (inout) Simple precision input/output matrix of (k * n) number of elements stored using 1D column-major
 *  \param nIter: (input) Number of iterations
 *  \return: 0 if all is OK, -1 otherwise  
*/
int smlsa_cpu(const int m, const int n, const int k, const float *A, float *W, float *H, const int nIter)
{
  float *B=NULL, *C=NULL, *L=NULL;
  int   i;

   B = (float *)nmf_alloc(max(m,n) * k * sizeof *B, WRDLEN);
   C = (float *)nmf_alloc(max(m,n) * k * sizeof *C, WRDLEN);
   L = (float *)nmf_alloc(       k * k * sizeof *L, WRDLEN);

   if (B == NULL || C == NULL || L == NULL) { return -1; }

   for(i=0;i<nIter;i++)
   {
      /* ************************ Phase 1 *************************** */
      /* B=W'*A */	
      cblas_sgemm(CblasColMajor, CblasTrans,   CblasNoTrans, k, n, m, 1.0, W, m, A, m, 0.0, B, k);

      /* L=W'*W */
      cblas_sgemm(CblasColMajor, CblasTrans,   CblasNoTrans, k, k, m, 1.0, W, m, W, m, 0.0, L, k);

      /* C=L*H */
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, k, n, k, 1.0, L, k, H, k, 0.0, C, k);

      /* H=H(*)B(/)C */
      sdotdiv_cpu(k*n, B, C, H);

      /* ************************ Phase 2 *************************** */
      /* B=A*H' */
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans,   m, k, n, 1.0, A, m, H, k, 0.0, B, m);

      /* L=H*H' */
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans,   k, k, n, 1.0, H, k, H, k, 0.0, L, k);

      /* C=W*L */
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, k, k, 1.0, W, m, L, k, 0.0, C, m);

      /* W=W(*)B(/)C */
      sdotdiv_cpu(m*k, B, C, W);
   }

   nmf_free(B);
   nmf_free(C);
   nmf_free(L);

  return 0;
}
