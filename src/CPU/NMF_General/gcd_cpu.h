/**
 *  \file    gcd_cpu.h
 *  \brief   Header file for using the GCD algorithm  with CPU
 *  \author  P. San Juan
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \date    08/05/18
*/
#ifndef GCD_CPU_H
#define GCD_CPU_H

#include <utils_cpu.h>
#include "general_cpu.h" //Public interface of the library

/*support functions */
void dupdaSwDw(const int, const int, const double *, const double *, const double *, const int, double *__restrict__, double *__restrict__);
void supdaSwDw(const int, const int, const float  *, const float  *, const float  *, const int, float  *__restrict__, float  *__restrict__);

void dupdaShDh(const int, const int, const double *, const double *, const double *, double *__restrict__, double *__restrict__);
void supdaShDh(const int, const int, const float  *, const float  *, const float  *, float  *__restrict__, float  *__restrict__);

#endif
