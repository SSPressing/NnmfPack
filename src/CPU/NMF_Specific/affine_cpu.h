/**
 *  \file    affine_cpu.h
 *  \brief   Header file for using the multiplicative algorithm to solve the affine NMF model  with CPU
 *  \author  P. San Juan
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \date    08/05/18
*/
#ifndef AFFINE_CPU_H
#define AFFINES_CPU_H

#include <utils_cpu.h>
#include "specific_cpu.h"// Public interface of the library

/*support functions*/
void dnrm1Cols(const int m, const int n, double *__restrict__);
void snrm1Cols(const int m, const int n, float  *__restrict__);

#endif
