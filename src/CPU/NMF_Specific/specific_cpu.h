/**
 *  \file    specific_cpu.h
 *  \brief   Interface file for the Specific NMF decomposition algorithms
 *  \author  P. San Juan
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \date    08/05/18
*/
#ifndef SPECIFIC_CPU_H
#define SPECIFIC_CPU_H

int dbdiv_cpu(const int, const int, const int, const double *, double *, double *, const double, const int);
int sbdiv_cpu(const int, const int, const int, const float  *, float  *, float  *, const float,  const int);

int dbdivRestrict_cpu(const int, const int,    const int,    const double *, double *, double *, const double, 
                      const int, const double, const double, const unsigned short); 
int sbdivRestrict_cpu(const int, const int,    const int,    const float *,  float *,  float *,  const float, 
                      const int, const float,  const float,  const unsigned short);

int dAffine_cpu(const int, const int, const int, const double *, double *, double *, double *, const int);
int sAffine_cpu(const int, const int, const int, const float *,  float *,  float *,  float *,  const int);


/*error measuring functions*/
double derrorbd_cpu(const int, const int, const int, const double *, const double *, const double *, const double);
float  serrorbd_cpu(const int, const int, const int, const float  *, const float  *, const float  *, const float);

#endif
