/**
 *  \file    affine_cpu.c
 *  \brief   File with functions to  use the multiplicative algorithm to solve the affine NMF modelfor CPUs
 *  \author  P. San juan
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \date    08/05/18
*/

#include "affine_cpu.h"

//TODO documentar y optimizar
int dAffine_cpu(const int m, const int n, const int k, const double *A, double *W, double *H, double *w, const int nIter)
{
   double *afA=NULL, *N=NULL, *D=NULL, *one=NULL;
   int     i;
    
   afA = (double *)nmf_alloc(m * n * sizeof *afA, WRDLEN);
   N   = (double *)nmf_alloc(m * k * sizeof *N,   WRDLEN);
   D   = (double *)nmf_alloc(m * k * sizeof *D,   WRDLEN);
   one = (double *)nmf_alloc(n     * sizeof *one, WRDLEN);

   if (afA==NULL || N==NULL || D==NULL || one==NULL) { return -1; }

   dmemset_cpu(n, one, 1.0);
	
   for(i=0;i<nIter;i++)
   {
      //afA = W*H + w0*one';
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, afA, m);
      cblas_dger (CblasColMajor, m, n, 1, w, 1, one, 1, afA, m);
        
      //N = (A * H') 
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans, m, k, n, 1.0, A, m, H, k, 0.0, N, m);
        
      //D = (afA * H')
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans, m, k, n, 1.0, afA, m, H, k, 0.0, D, m);
        
      //W = W .* N ./ D;
      ddotdiv_cpu(m*k, N, D, W);
        
      //normalize columns using 1-norm
      dnrm1Cols(m, n, W);
        
      //N =( W' * A)
      cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, n, m, 1.0, W, m, A, m, 0.0, N, k);	

      //D = (W' * afA)
      cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, n, m, 1.0, W, m, afA, m, 0.0, D, k);	

      //H = H .* N ./ D;
      ddotdiv_cpu(k*n, N, D, H);
        
      //n = (A * one) reutilizamos N Y D para ahorrar memoria en lugar de declarar nuevos buffers para los vectores n y d
      cblas_dgemv(CblasColMajor, CblasNoTrans, m, n, 1, A,   m, one, 1, 0, N, 1);

      //d = (afA * one)
      cblas_dgemv(CblasColMajor, CblasNoTrans, m, n, 1, afA, m, one, 1, 0, D, 1);

      //w = w .* n ./ d;
      ddotdiv_cpu(m, N, D, w);
   }

   nmf_free(afA);
   nmf_free(N);
   nmf_free(D);
   nmf_free(one);

   return 0;
}


//TODO documentar y optimizar
int sAffine_cpu(const int m, const int n, const int k, const float *A, float *W, float *H, float *w, const int nIter)
{
   float *afA=NULL, *N=NULL, *D=NULL, *one=NULL;
   int    i;
    
   afA = (float *)nmf_alloc(m * n * sizeof *afA, WRDLEN);
   N   = (float *)nmf_alloc(m * k * sizeof *N,   WRDLEN);
   D   = (float *)nmf_alloc(m * k * sizeof *D,   WRDLEN);
   one = (float *)nmf_alloc(n     * sizeof *one, WRDLEN);

   if (afA==NULL || N==NULL || D==NULL || one==NULL) { return -1; }

   smemset_cpu(n, one, 1.0);
	
   for(i=0;i<nIter;i++)
   {
      //afA = W*H + w0*one';
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, W, m, H, k, 0.0, afA, m);
      cblas_sger (CblasColMajor, m, n, 1, w, 1, one, 1, afA, m);
        
      //N = (A * H') 
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans, m, k, n, 1.0, A, m, H, k, 0.0, N, m);
        
      //D = (afA * H')
      cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans, m, k, n, 1.0, afA, m, H, k, 0.0, D, m);
        
      //W = W .* N ./ D;
      sdotdiv_cpu(m*k, N, D, W);
        
      //normalize columns using 1-norm
      snrm1Cols(m, n, W);
        
      //N =( W' * A)
      cblas_sgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, n, m, 1.0, W, m, A, m, 0.0, N, k);	

      //D = (W' * afA)
      cblas_sgemm(CblasColMajor, CblasTrans, CblasNoTrans, k, n, m, 1.0, W, m, afA, m, 0.0, D, k);	

      //H = H .* N ./ D;
      sdotdiv_cpu(k*n, N, D, H);
        
      //n = (A * one) reutilizamos N Y D para ahorrar memoria en lugar de declarar nuevos buffers para los vectores n y d
      cblas_sgemv(CblasColMajor, CblasNoTrans, m, n, 1, A,   m, one, 1, 0, N, 1);

      //d = (afA * one)
      cblas_sgemv(CblasColMajor, CblasNoTrans, m, n, 1, afA, m, one, 1, 0, D, 1);

      //w = w .* n ./ d;
      sdotdiv_cpu(m, N, D, w);
   }

   nmf_free(afA);
   nmf_free(N);
   nmf_free(D);
   nmf_free(one);

   return 0;
}


//TODO documentar
void dnrm1Cols(const int m, const int n, double *__restrict__ M)
{
   int i;
   double norm;
    
   #ifdef With_ICC
     #pragma loop_count min=32
     #pragma omp simd
   #else
     #pragma omp parallel for
   #endif
   for (i=0; i<n; i++)
   {
      norm = cblas_dasum(m, &M[i*m], 1) + dEPS;
      cblas_dscal(m, 1.0/norm, &M[i*m], 1);
   }
}


//TODO documentar
void snrm1Cols(const int m, const int n, float *__restrict__ M)
{
   int i;
   float norm;
    
   #ifdef With_ICC
     #pragma loop_count min=32
     #pragma omp simd
   #else
     #pragma omp parallel for
   #endif
   for (i=0; i<n; i++)
   {
      norm = cblas_sasum(m, &M[i*m], 1) + sEPS;
      cblas_sscal(m, 1.0/norm, &M[i*m], 1);
   }
}
