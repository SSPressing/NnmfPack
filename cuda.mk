
.cu.o:
	$(NVCC)  -o $@ -c $< --compiler-options=" $(default_gcc_cflags) $(use_mkl_flag) -fopenmp " $(AM_CPPFLAGS) $(CUDA_CFLAGS) $(CUDA_INCLUDE) 
.cu.lo:
	$(top_srcdir)/cudalt.py $@ $(NVCC) $(CUDA_CFLAGS) --compiler-options=\" $(default_gcc_cflags) $(use_mkl_flag) -fopenmp $(DEFAULT_INCLUDES) $(INCLUDES) $(CUDA_INCLUDE) $(AM_CPPFLAGS) $(CPPFLAGS) \" -c $<
