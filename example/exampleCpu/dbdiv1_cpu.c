/***************************************************************************
 *   Copyright (C) 2014 by PIR (University of Oviedo) and                  *
 *   INCO2 (Polytechnic University of Valencia) groups.                    *
 *   nmfpack@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************
*/
/**
 *  \file    dbdiv1_cpu.c
 *  \brief   Beta Divergence (bdiv) example of usage. Matrices A, W and H are
 *           randomly generated. Double precision is used. Layout 1D column-mayor.
 *  \author  Information Retrieval and Parallel Computing Group (IRPCG)
 *  \author  University of Oviedo, Spain
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \author  Contact: nmfpack@gmail.com
 *  \date    04/11/14
*/
#include <nnmfpackCpu.h>

int main(int argc, char *argv[])
{
   int status, iters, seed,
       m,      /* number of rows   of matrix A */
       n,      /* number of colums of matrix A */
       k;      /* number of colums of matrix W and number of rows of matrix H   */

   double *A=NULL, *W=NULL, *H=NULL, beta =2.0, error=0.0, Time =0.0;

   if (argc != 7)
   {
     printf("Apply Beta-Divergence. A, W and H are randomly generated double precision matrices.\n");
     printf("\tA: is an m x n matrix.\n");
     printf("\tW: is an m x k matrix.\n");
     printf("\tH: is an k x n matrix.\n");
     printf("\tbeta: beta value.\n");
     printf("\titer: number of iterations.\n");
     printf("\tseed: seed for the Pseudo Random Generator.\n");
     printf("Usage: %s <m> <n> <k> <beta> <iter> <seed>\n", argv[0]);
     return -1;
   }

   m    =atoi(argv[1]);
   n    =atoi(argv[2]);  
   k    =atoi(argv[3]);
   beta =atof(argv[4]);
   iters=atoi(argv[5]);
   seed =atoi(argv[6]);
  
   if ((m <= 0) || (n <= 0))
   {
     printf("Input matrix A incorrect: null or negative dimension\n");
     return -1;
   }

   if ((k > m) || (k > n))
   {
     printf("k must be less than min(n,m)\n");
     return -1;
   }

   A = (double *)nmf_alloc(m * n * sizeof *A, WRDLEN);
   W = (double *)nmf_alloc(m * k * sizeof *W, WRDLEN);
   H = (double *)nmf_alloc(k * n * sizeof *H, WRDLEN);

   if (A==NULL || W==NULL || H==NULL) { return -1; } 

   /* Choose random A, W, H */
   dlarngenn_cpu(m, n, seed+11, A);
   dlarngenn_cpu(m, k, seed+13, W);
   dlarngenn_cpu(k, n, seed+17, H);

   /* Calling beta-divergence CPU function */
   #ifdef With_OMP
     Time = omp_get_wtime();
   #endif

   /* uType=1 --> Update W and H */
   /* uType=2 --> Only update W  */
   /* uType=3 --> Only update H  */    
   status = dbdiv_cpu(m, n, k, A, W, H, beta, iters);

   #ifdef With_OMP
     Time = omp_get_wtime() - Time;
   #endif

   if (status != 0)
   {
     printf("Problems with Beta-Divergence. Error code: %d\n", status);
     return -1;
   }

   /* Frobenious norm or Beta_divergence error based. */
   /* Choose one.                                     */
   /* error=derror_cpu(m, n, k, A, W, H);             */
   error=derrorbd_cpu(m, n, k, A, W, H, beta);
      
   printf("#  M    N     K      Beta       Iteration    Time (sec)      Error\n");
   printf("# --------------------------------------------------------------------\n");     
   printf("%4d  %4d  %4d    %1.6f    %4d        %1.5E    %1.8E\n", m, n, k, beta, iters, Time, error);

   nmf_free(A);
   nmf_free(W);
   nmf_free(H);

   return 0;
}
