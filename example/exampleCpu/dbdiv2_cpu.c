/***************************************************************************
 *   Copyright (C) 2014 by PIR (University of Oviedo) and                  *
 *   INCO2 (Polytechnic University of Valencia) groups.                    *
 *   nmfpack@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************
*/
/**
 *  \file    dbdiv2_cpu.c
 *  \brief   Beta Divergence (bdiv) example of usage. It reads matrices A, W
 *           and H from files and writes updated matrices W and H to files.
 *           Double precision is used. Layout 1D column-mayor.
 *  \author  Information Retrieval and Parallel Computing Group (IRPCG)
 *  \author  University of Oviedo, Spain
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \author  Contact: nmfpack@gmail.com
 *  \date    04/11/14
*/
#include <nnmfpackCpu.h>
#include <utils.h>
#include <string.h>

int main(int argc, char *argv[])
{
   int rows_A, rows_W, rows_H, cols_A, cols_W, cols_H, status, iters;

   double *A=NULL, *W=NULL, *H=NULL, beta =2.0, error=0.0, Time =0.0;

   char *Path_A=NULL, *Path_W=NULL, *Path_H=NULL;

   char  Path_W_Out[500], Path_H_Out[500], Path_WH_Out[500];

   if (argc != 6)
   {
     printf("Apply Beta-Divergence to the input double precision matrices A, W and H. Write updated matrices W and H to files.\n");
     printf("\tA: is an m x n matrix.\n");
     printf("\tW: is an m x k matrix.\n");
     printf("\tH: is an k x n matrix.\n");
     printf("\tbeta: beta value.\n");
     printf("\titer:  number of iterations.\n");
     printf("Usage: %s <A-file> <W-file> <H-file> <beta> <iter>\n", argv[0]);
     return -1;
   }

   Path_A=argv[1];
   Path_W=argv[2];
   Path_H=argv[3];
   beta  =atof(argv[4]);
   iters =atoi(argv[5]);
  
   if (read_file_header(Path_A, &rows_A, &cols_A) < 0)
   {
     printf("Cannot open A matrix file: %s \n", Path_A);
     return -1;
   }

   if ((rows_A <= 0) || (cols_A <= 0))
   {
     printf("Input matrix A incorrect: null or negative dimension\n");
     return -1;
   }

   if (read_file_header(Path_W, &rows_W, &cols_W) < 0)
   {
     printf("Cannot open W matrix file: %s \n", Path_W);
     return -1;
   }

   if ((rows_W <= 0) || (cols_W <= 0))
   {
     printf("Input matrix W incorrect: null or negative dimension\n");
     return -1;
   }

   if (read_file_header(Path_H, &rows_H, &cols_H) < 0)
   {
     printf("Cannot open H matrix file: %s \n", Path_H);
     return -1;
   }

   if ((rows_H <= 0) || (cols_H <= 0))
   {
     printf("Input matrix H incorrect: null or negative dimension\n");
     return -1;
   }

   if (rows_A != rows_W)
   {
     printf("A rows should be equal to W rows\n");
     return -1;
   }

   if (cols_A != cols_H)
   {
     printf("A columns should be equal to H columns\n");
     return -1;
   }

   if (cols_W != rows_H)
   {
     printf("W columns should be equal to H rows\n");
     return -1;
   }
  
   strcpy(Path_W_Out,  Path_W);
   strcpy(Path_H_Out,  Path_H);
   strcpy(Path_WH_Out, Path_A);
  
   strcat(Path_W_Out,  "_out");
   strcat(Path_H_Out,  "_out");
   strcat(Path_WH_Out, "_out");

   A = (double *)nmf_alloc(rows_A * cols_A * sizeof *A, WRDLEN);
   W = (double *)nmf_alloc(rows_W * cols_W * sizeof *W, WRDLEN);
   H = (double *)nmf_alloc(rows_H * cols_H * sizeof *H, WRDLEN);

   if (A==NULL || W==NULL || H==NULL) { return -1; } 
 
   if (dread_file(Path_A, rows_A, cols_A, A) < 0)
   {
     printf("Cannot open A matrix file: %s \n", Path_A);
     return -1;
   }

   if (dread_file(Path_W, rows_W, cols_W, W) < 0)
   {
     printf("Cannot open W matrix file: %s \n", Path_W);
     return -1;
   }

   if (dread_file(Path_H, rows_H, cols_H, H) < 0)
   {
     printf("Cannot open H matrix file: %s \n", Path_H);
     return -1;
   }

   /* Calling beta-divergence CPU function */
   #ifdef With_OMP
     Time = omp_get_wtime();
   #endif

   /* uType=1 --> Update W and H */
   /* uType=2 --> Only update W  */
   /* uType=3 --> Only update H  */    
   status = dbdiv_cpu(rows_A, cols_A, rows_H, A, W, H, beta, iters);

   #ifdef With_OMP
     Time = omp_get_wtime() - Time;
   #endif

   if (status != 0)
   {
     printf("Problems with Beta-Divergence. Error code: %d\n", status);
     return -1;
   }

   /* Frobenious norm or Beta_divergence error based.    */
   /* Choose one.                                        */
   /* error=derror_cpu(rows_A, cols_A, rows_H, A, W, H); */
   error=derrorbd_cpu(rows_A, cols_A, rows_H, A, W, H, beta);
    
   printf("#  M    N     K      Beta       Iteration    Time (sec)      Error   \n");
   printf("# -------------------------------------------------------------------\n");     
   printf("%4d  %4d  %4d    %1.6f    %4d        %1.5E    %1.8E\n", rows_A, cols_A, rows_H, beta, iters, Time, error);

   cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, rows_W, cols_H, rows_H, 1.0, W, rows_W, H, rows_H, 0.0, A, rows_A);

   dwrite_file(Path_W_Out,  &rows_W, &cols_W, W);
   dwrite_file(Path_H_Out,  &rows_H, &cols_H, H);
   dwrite_file(Path_WH_Out, &rows_A, &cols_A, A);

   printf("Output file W  saved as %s\n",  Path_W_Out);
   printf("Output file H  saved as %s\n",  Path_H_Out);
   printf("Output file WH saved as %s\n",  Path_WH_Out);

   nmf_free(A);
   nmf_free(W);
   nmf_free(H);
 
   return 0;
}
