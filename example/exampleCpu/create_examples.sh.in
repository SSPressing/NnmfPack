#! /bin/sh
# *   Copyright (C) 2014 by PIR (University of Oviedo) and INCO2 (Univesity *
# *   of Valencia) groups                                                   *
# *   nmfpack@gmail.com                                                     *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU General Public License for more details.                          *
# *                                                                         *
# *   You should have received a copy of the GNU General Public License     *
# *   along with this program; if not, write to the                         *
# *   Free Software Foundation, Inc.,                                       *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
# ***************************************************************************
#*/
#
#/**
# *  \file    create_examples.sh.in
# *  \brief   script for compiling examples of usage for CPUs
# *  \author  Information Retrieval and Parallel Computing Group (IRPCG)
# *  \author  University of Oviedo, Spain
# *  \author  Interdisciplinary Computation and Communication Group (INCO2)
# *  \author  Universitat Politecnica de Valencia, Spain.
# *  \author  Contact: nmfpack@gmail.com
# *  \date    04/11/14
#*/

prefix="@prefix@"
exec_prefix="@exec_prefix@"

NNMFPACK_INCLUDE_DIR="@includedir@"
NNMFPACK_LIB_DIR="@libdir@"
NNMFPACK_LIB_FLAGS="-lnnmfpack"

CC="@CC@"
CFLAGS="@CFLAGS@"
OPENMP_CC_FLAGS="@openmp_cc_flags@"
USE_FLAG_CPU="@use_cpu_flag@"
CBLAS_LIBS="@cblas_libs@"
CBLAS_INCS="@cblas_incs@"

USE_MKL_FLAG="@use_mkl_flag@"
MKL_INCLUDE_DIR_FLAG="@mkl_include_dir_flag@"
MKL_CC_LIB_FLAGS="@mkl_cc_lib_flags@"
MKL_LIB_DIR_FLAG="@mkl_lib_dir_flag@"


progsd="dbdiv1_cpu sbdiv1_cpu dbdiv2_cpu sbdiv2_cpu"


for i in $progsd
do
 $CC -o $i $i.c utils.c -I.\
        -I$NNMFPACK_INCLUDE_DIR $CFLAGS \
        $CBLAS_INCS \
	-L$NNMFPACK_LIB_DIR \
	$NNMFPACK_LIB_FLAGS \
	$USE_MKL_FLAG \
	$MKL_INCLUDE_DIR_FLAG \
	$MKL_CC_LIB_FLAGS \
	$MKL_LIB_DIR_FLAG \
	$CBLAS_LIBS \
	$OPENMP_CC_FLAGS \
	$USE_FLAG_CPU
done
