/***************************************************************************
 *   Copyright (C) 2014 by PIR (University of Oviedo) and                  *
 *   INCO2 (Polytechnic University of Valencia) groups.                    *
 *    nmfpack@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************
*/
/**
 *  \file    sbdiv2_gpu.cu
 *  \brief   Beta Divergence (bdiv) example of usage. It reads matrices A, W
 *           and H from files and writes updated matrices W and H to files;
 *           Simple precision is used. Layout 1D column-mayor.
 *  \author  Information Retrieval and Parallel Computing Group (IRPCG)
 *  \author  University of Oviedo, Spain
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \author  Contact: nmfpack@gmail.com
 *  \date    04/11/14
*/
#include <nnmfpackGpu.h>
#include <utils.h>

int main(int argc, char *argv[])
{
  int
    rows_A, rows_W, rows_H,
    cols_A, cols_W, cols_H,
    status, iters, devID,
    uType;  /* What to update? W and H (uType=1), W (uType=2) or H (uType=3) */

  float
    *d_A=NULL,   /* Inside device */
    *d_W=NULL,   /* Inside device */
    *d_H=NULL,   /* Inside device */	
    *A  =NULL,   /* In CPU */
    *W  =NULL,   /* In CPU */
    *H  =NULL,   /* In CPU */	
    beta =2.0f,
    error=0.0f,
    Time =0.0f;

  char
    *Path_A=NULL,
    *Path_W=NULL,
    *Path_H=NULL;

  char
    Path_W_Out[500],
    Path_H_Out[500],
    Path_WH_Out[500];

  cudaDeviceProp
    deviceProp;

  cudaEvent_t
    start, stop;

  cublasHandle_t
    handle;

  if (argc != 7)
  {
    printf("Apply Beta-Divergence to the input simple precision matrices A, W and H. Write updated matrices W and H to files.\n");
    printf("\tA: is an m x n matrix.\n");
    printf("\tW: is an m x k matrix.\n");
    printf("\tH: is an k x n matrix.\n");
    printf("\tbeta: beta value.\n");
    printf("\tuType: 1 for update W and H; 2 for update W only; 3 for update H only.\n");
    printf("\titer:  number of iterations.\n");
    printf("Usage: %s <A-file> <W-file> <H-file> <beta> <uType> <iter>\n", argv[0]);
    return -1;
  }

  Path_A=argv[1];
  Path_W=argv[2];
  Path_H=argv[3];
  beta  =(float)atof(argv[4]);
  uType =atoi(argv[5]);
  iters =atoi(argv[6]);
  
  if (read_file_header(Path_A, &rows_A, &cols_A) < 0)
  {
    printf("Cannot open A matrix file: %s \n", Path_A);
    return -1;
  }

  if ((rows_A <= 0) || (cols_A <= 0))
  {
    printf("Input matrix A incorrect: null or negative dimension\n");
    return -1;
  }

  if (read_file_header(Path_W, &rows_W, &cols_W) < 0)
  {
    printf("Cannot open W matrix file: %s \n", Path_W);
    return -1;
  }

  if ((rows_W <= 0) || (cols_W <= 0))
  {
    printf("Input matrix W incorrect: null or negative dimension\n");
    return -1;
  }

  if (read_file_header(Path_H, &rows_H, &cols_H) < 0)
  {
    printf("Cannot open H matrix file: %s \n", Path_H);
    return -1;
  }

  if ((rows_H <= 0) || (cols_H <= 0))
  {
    printf("Input matrix H incorrect: null or negative dimension\n");
    return -1;
  }

  if (rows_A != rows_W)
  {
    printf("A rows should be equal to W rows\n");
    return -1;
  }

  if (cols_A != cols_H)
  {
    printf("A columns should be equal to H columns\n");
    return -1;
  }

  if (cols_W != rows_H)
  {
    printf("W columns should be equal to H rows\n");
    return -1;
  }
  
  strcpy(Path_W_Out,  Path_W);
  strcpy(Path_H_Out,  Path_H);
  strcpy(Path_WH_Out, Path_A);
  
  strcat(Path_W_Out,  "_out");
  strcat(Path_H_Out,  "_out");
  strcat(Path_WH_Out, "_out");
 
  CUDAERR(cudaGetDevice(&devID));
  CUDAERR(cudaGetDeviceProperties(&deviceProp, devID));
  printf("Using GPU %d (\"%s\") with compute capability %d.%d\n\n", devID, deviceProp.name, deviceProp.major, deviceProp.minor);

  CUDAERR(cudaHostAlloc((void **)&A, rows_A * cols_A * sizeof(float), cudaHostAllocDefault));
  CUDAERR(cudaHostAlloc((void **)&W, rows_W * cols_W * sizeof(float), cudaHostAllocDefault));
  CUDAERR(cudaHostAlloc((void **)&H, rows_H * cols_H * sizeof(float), cudaHostAllocDefault));

  CUDAERR(cudaMalloc((void **)&d_A, rows_A * cols_A * sizeof(float)));
  CUDAERR(cudaMalloc((void **)&d_W, rows_W * cols_W * sizeof(float)));
  CUDAERR(cudaMalloc((void **)&d_H, rows_H * cols_H * sizeof(float)));

  if (sread_file(Path_A, rows_A, cols_A, A) < 0)
  {
    printf("Cannot open A matrix file: %s \n", Path_A);
    return -1;
  }

  if (sread_file(Path_W, rows_W, cols_W, W) < 0)
  {
    printf("Cannot open W matrix file: %s \n", Path_W);
    return -1;
  }

  if (sread_file(Path_H, rows_H, cols_H, H) < 0)
  {
    printf("Cannot open H matrix file: %s \n", Path_H);
    return -1;
  }

  CUDAERR(cudaMemcpy(d_A, A, rows_A * cols_A * sizeof(float), cudaMemcpyHostToDevice));
  CUDAERR(cudaMemcpy(d_W, W, rows_W * cols_W * sizeof(float), cudaMemcpyHostToDevice));
  CUDAERR(cudaMemcpy(d_H, H, rows_H * cols_H * sizeof(float), cudaMemcpyHostToDevice));

  CUDAERR(cudaEventCreate(&start));
  CUDAERR(cudaEventCreate(&stop));

  /* Calling beta-divergence GPU function */
  CUDAERR(cudaEventRecord(start, NULL));
    /* uType=1 --> Update W and H */
    /* uType=2 --> Only update W  */
    /* uType=3 --> Only update H  */    
    status = sbdiv_cuda(rows_A, cols_A, rows_H, d_A, d_W, d_H, beta, uType, iters);
  CUDAERR(cudaEventRecord(stop, NULL));

  CUDAERR(cudaEventSynchronize(stop));
  CUDAERR(cudaEventElapsedTime(&Time, start, stop));

  if (status != 0)
  {
    printf("Problems with Beta-Divergence. Error code: %d\n", status);
    return -1;
  }

  /* Frobenious norm or Beta_divergence error based. */
  /* Choose one.                                     */
  /* error=serror_cuda(rows_A, cols_A, rows_H, d_A, d_W, d_H); */
  error=serrorbd_cuda(rows_A, cols_A, rows_H, d_A, d_W, d_H, beta);

  printf("#  M    N     K      Beta       Iteration    Time (sec)      Error   Update Model\n");
  printf("# -----------------------------------------------------------------------------------\n");     
  printf("%4d  %4d  %4d    %1.6f    %4d        %1.5E    %1.8E    %1d\n", rows_A, cols_A, rows_H, beta, iters, Time/1000.0f, error, uType);

  error=1.0f; beta=0.0f;
  CUBLASERR(cublasCreate(&handle));
  CUBLASERR(cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, rows_W, cols_H, rows_H, &error, W, rows_W, H, rows_H, &beta, d_A, rows_A));
  CUBLASERR(cublasDestroy(handle));

  CUDAERR(cudaMemcpy(W, d_W, rows_W * cols_W * sizeof(float), cudaMemcpyDeviceToHost));
  CUDAERR(cudaMemcpy(H, d_H, rows_H * cols_H * sizeof(float), cudaMemcpyDeviceToHost));
  CUDAERR(cudaMemcpy(A, d_A, rows_A * cols_A * sizeof(float), cudaMemcpyDeviceToHost));

  swrite_file(Path_W_Out,  &rows_W, &cols_W, W);
  swrite_file(Path_H_Out,  &rows_H, &cols_H, H);
  swrite_file(Path_WH_Out, &rows_A, &cols_A, A);

  printf("Output file W  saved as %s\n",  Path_W_Out);
  printf("Output file H  saved as %s\n",  Path_H_Out);
  printf("Output file WH saved as %s\n",  Path_WH_Out);

  CUDAERR(cudaFree(d_A));
  CUDAERR(cudaFree(d_W));
  CUDAERR(cudaFree(d_H));
  CUDAERR(cudaFreeHost(A));
  CUDAERR(cudaFreeHost(W));
  CUDAERR(cudaFreeHost(H));

  CUDAERR(cudaDeviceReset());

  return 0;
}
