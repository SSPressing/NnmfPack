function test_driver2(matA, matWin, matHin, matWout, matHout, beta, numIters);
  %% matA    path to the file with A matrix
  %% matWin  path to the file with input  W matrix
  %% matHin  path to the file with input  H matrix
  %% matWout path to the file with output W matrix obtained with external call to nnmfpack using A, Win and Hin matrices
  %% matHout path to the file with output H matrix obtained with external call to nnmfpack using A, Win and Hin matrices
  %% beta is the beta-divergencia value for beta
  %% uType=1 --> Update W and H
  %% uType=2 --> Only update W
  %% uType=3 --> Only update H
  %% numIters is the number of iterations

  [FID,MSG]=fopen(matA, 'r');
  m=fread(FID,1,'int');
  n=fread(FID,1,'int');
  A=fread(FID,[m,n],'double');
  fclose(FID);

  [FID,MSG]=fopen(matWin, 'r');
  m=fread(FID,1,'int');
  k=fread(FID,1,'int');
  W0=fread(FID,[m,k],'double');
  fclose(FID);

  [FID,MSG]=fopen(matHin, 'r');
  k=fread(FID,1,'int');
  n=fread(FID,1,'int');
  H0=fread(FID,[k,n],'double');
  fclose(FID);

  [FID,MSG]=fopen(matWout, 'r');
  m=fread(FID,1,'int');
  k=fread(FID,1,'int');
  WE=fread(FID,[m,k],'double');
  fclose(FID);

  [FID,MSG]=fopen(matHout, 'r');
  k=fread(FID,1,'int');
  n=fread(FID,1,'int');
  HE=fread(FID,[k,n],'double');
  fclose(FID);

  tic();
    [W,H]=mex_bdivCpu(A, W0, H0, beta, numIters);
  time=toc();

  disp(sprintf('NnmfPack with (m, n, k, beta, numIters, Time)=(%d, %d, %d, %f, %d, %f)\n', m, n, k, beta, numIters, time));

  norma=norm(W - WE);
  disp(sprintf('||W-Wref||=%f\n', norma));
  norma=norm(H - HE);
  disp(sprintf('||H-Href||=%f\n', norma));

  exit;
