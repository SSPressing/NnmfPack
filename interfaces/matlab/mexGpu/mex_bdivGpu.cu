/***************************************************************************
 *   Copyright (C) 2014 by PIR (University of Oviedo) and INCO2 (Univesity *
 *   of Valencia) groups                                                   *
 *   nmfpack@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************
*/
/**
 *  \file    mex_bdivGpu.cu
 *  \brief   mex_bdivGpu is the nnmfpack's GPU-driver (nnmfpack's functions runs
 *           on GPUs) for using it from from Matlab/Octave. Layout 1D column-mayor.
 *  \author  Information Retrieval and Parallel Computing Group (IRPCG)
 *  \author  University of Oviedo, Spain
 *  \author  Interdisciplinary Computation and Communication Group (INCO2)
 *  \author  Universitat Politecnica de Valencia, Spain.
 *  \author  Contact: nmfpack@gmail.com     
 *  \date    04/11/14
*/

#include <nnmfpackGpu.h>
#include <string.h>
#include "mex.h"

/**
 *  \fn    void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
 *  \brief Standar MEX interface. Double precision.
 *  \param nlhs: (output) Number of expected output positions of mxArrays with data
 *  \param plhs: (output) Outpur parameters. They are: W and H matrices
 *  \param nrhs: (input)  Number of input positions of mxArray with data
 *  \param prhs: (input)  Input parameters. They are: A, W and H matrices, number of iterations and value of beta
*/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  int
    m, n, k, nIts, uType;
    
  double
    *A=NULL, *W=NULL, *W0=NULL, *H=NULL, *H0=NULL, beta;

  double
    *tmp_A=NULL, *d_A=NULL, *tmp_W=NULL, *d_W=NULL, *tmp_H=NULL, *d_H=NULL;
    
  if (nrhs==6 && nlhs==2)
  {
    /* mexPrintf("mex_bdivGpu: start\n"); */

    /* Input parameters: mxGetN --> number of columns; mxGetM --> number of rows */
    m = mxGetM(prhs[0]);
    n = mxGetN(prhs[0]);
    k = mxGetN(prhs[1]);

    A  = mxGetPr(prhs[0]);
    W0 = mxGetPr(prhs[1]);
    H0 = mxGetPr(prhs[2]);

    beta = mxGetScalar(prhs[3]);
    uType= (int)mxGetScalar(prhs[4]);
    nIts = (int)mxGetScalar(prhs[5]);
 
    /* Output parameters */
    plhs[0] = mxCreateDoubleMatrix(m,k, mxREAL);
    plhs[1] = mxCreateDoubleMatrix(k,n, mxREAL);
  
    W = mxGetPr(plhs[0]);
    H = mxGetPr(plhs[1]);

    cudaHostAlloc((void **)&tmp_A, mxGetElementSize(prhs[0])*m*n,cudaHostAllocMapped);
    cudaHostAlloc((void **)&tmp_W, mxGetElementSize(prhs[1])*m*k,cudaHostAllocMapped);
    cudaHostAlloc((void **)&tmp_H, mxGetElementSize(prhs[2])*k*n,cudaHostAllocMapped);
      
    memcpy(tmp_A, A,  m*n*mxGetElementSize(prhs[0]));
    memcpy(tmp_W, W0, m*k*mxGetElementSize(prhs[1]));
    memcpy(tmp_H, H0, k*n*mxGetElementSize(prhs[2]));

    cudaHostGetDevicePointer((void **)&d_A, (void *)tmp_A, 0);
    cudaHostGetDevicePointer((void **)&d_W, (void *)tmp_W, 0);
    cudaHostGetDevicePointer((void **)&d_H, (void *)tmp_H, 0);

    dbdiv_cuda(m, n, k, d_A, d_W, d_H, beta, uType, nIts);

    memcpy(W, tmp_W, m*k*mxGetElementSize(prhs[1]));
    memcpy(H, tmp_H, k*n*mxGetElementSize(prhs[2]));
  
    /* mexPrintf("mex_bdivGpu end\n"); */
  }
  else
    mexPrintf("The number of inputs/expected-output positions of mex_bdivCpu are wrong\n"); 
} 
